package inheritance.analyser;

import java.util.List;

public non-sealed class FlatTaxSalesAnalyser extends AbstractSalesAnalyser{

    private double totalSales;
    private double totalSalesByProductId;


    public FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double getTotalSales() {
        for (SalesRecord sales : records) {
            totalSales += sales.getProductPrice() * sales.getItemsSold()  / 1.2;
        }
        return totalSales;
    }

    @Override
    protected Double getTotalSalesByProductId(String id) {
        for (SalesRecord sales : records) {
            if (sales.getProductId().equals(id)) {
                totalSalesByProductId += sales.getProductPrice() * sales.getItemsSold() / 1.2;
            }
        }
        return totalSalesByProductId;
    }

}
