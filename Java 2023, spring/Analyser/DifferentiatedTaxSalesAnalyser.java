package inheritance.analyser;

import java.util.List;

public non-sealed class DifferentiatedTaxSalesAnalyser extends AbstractSalesAnalyser{

    private  double totalSales;
    private double totalSalesByProductId;

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double getTotalSales() {
        for (SalesRecord sales : records) {
            if (sales.hasReducedRate()) {
                totalSales += sales.getProductPrice() * sales.getItemsSold() / 1.1;
            }
            else {
                totalSales += sales.getProductPrice() * sales.getItemsSold() / 1.2;
            }
        }
        return totalSales;
    }

    @Override
    protected Double getTotalSalesByProductId(String id) {
        for (SalesRecord sales : records) {
            if (sales.getProductId().equals(id)) {
                if (sales.hasReducedRate()) {
                    totalSalesByProductId += sales.getProductPrice() * sales.getItemsSold() / 1.1;
                } else {
                    totalSalesByProductId += sales.getProductPrice() * sales.getItemsSold() / 1.2;
                }
            }
        }
        return totalSalesByProductId;
    }

}
