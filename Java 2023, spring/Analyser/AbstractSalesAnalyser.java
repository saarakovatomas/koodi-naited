package inheritance.analyser;

import java.util.List;

public abstract sealed class AbstractSalesAnalyser permits DifferentiatedTaxSalesAnalyser, FlatTaxSalesAnalyser, TaxFreeSalesAnalyser {
    protected final List<SalesRecord> records;

    public AbstractSalesAnalyser(List<SalesRecord> records) {
        this.records = records;

    }

    protected abstract Double getTotalSales();

    protected abstract Double getTotalSalesByProductId(String id);

    protected String getIdOfMostPopularItem() {
        String mostPopularItemString = "";
        int mostPopular = 0;
        for (SalesRecord sales : records) {
            if (sales.getItemsSold() > mostPopular) {
                mostPopular += sales.getItemsSold();
                mostPopularItemString = sales.getProductId();
            }
        }
        return mostPopularItemString;
    }

    protected String getIdOfItemWithLargestTotalSales() {
        String largestTotalSalesString = "";
        int largestTotalSales = 0;
        for (SalesRecord sales : records) {
            int totalSale = sales.getItemsSold() * sales.getProductPrice();
            if (totalSale > largestTotalSales) {
                largestTotalSales += sales.getItemsSold();
                largestTotalSalesString = sales.getProductId();
            }
        }
        return largestTotalSalesString;
    }
}
