package inheritance.analyser;

import java.util.List;

public non-sealed class TaxFreeSalesAnalyser extends AbstractSalesAnalyser {

    public TaxFreeSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double getTotalSales() {
        double totalSales = 0;
        for (SalesRecord sales : records) {
            totalSales += sales.getProductPrice() * sales.getItemsSold();
        }
        return totalSales;
    }

    @Override
    protected Double getTotalSalesByProductId(String id) {
        double totalSalesByProductId = 0;
        for (SalesRecord sales : records) {
            if (sales.getProductId().equals(id)) {
                totalSalesByProductId += sales.getProductPrice() * sales.getItemsSold();
            }
        }
        return totalSalesByProductId;
    }

}
