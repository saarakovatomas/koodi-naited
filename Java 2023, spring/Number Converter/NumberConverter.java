package exceptions.numbers;

import exceptions.basic.Resource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {
    Properties properties = new Properties();

    public static void main(String[] args) {
        NumberConverter converter = new NumberConverter("et");
        System.out.println(converter.numberInWords(120));
    }

    public NumberConverter(String lang) {
        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);

        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(is, StandardCharsets.UTF_8);

            properties.load(reader);
        }
        catch (IOException e) {
            throw new MissingLanguageFileException(String.format("Language file for %s is missing ", lang));
        }

        catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException(String.format("Language file for %s is broken ", lang));
        }
        catch (Exception e) {
            throw new RuntimeException("not implemented yet");
        }
        finally {
            close(is);
        }
        if (properties == null || properties.isEmpty()) {
            throw new MissingTranslationException(String.format("Translation for %s is missing ", lang));
        }

        System.out.println(properties.containsKey(String.valueOf(1)));
        System.out.println(properties.getProperty(String.valueOf(1)));

    }

    public String numberInWords(Integer number) {
        String resultStringNumber;
        if (number >= 100 ) {
            resultStringNumber = numberInWordsHundred(number);
        }
        else {
            if (number <= 99 && number > 10) {
                resultStringNumber = numberInWordsTens(number);
            }
            else {
                resultStringNumber = properties.getProperty(String.valueOf(number));
            }
        }
        return resultStringNumber;
    }

    public String numberInWordsHundred(Integer number) {
        StringBuilder stringNumber = new StringBuilder();
        int tens = number % 100;
        int ones = tens % 10;
        stringNumber.append(numberInWordsHundredPart(number));
        if (tens > 0) {
            if (tens < 10) {
                stringNumber.append(properties.getProperty(String.valueOf(tens)));
            }
            else if (tens < 20) {
                if (properties.containsKey(String.valueOf(tens))) {
                    stringNumber.append(properties.getProperty(String.valueOf(tens)));
                }
                else {
                    stringNumber.append(properties.getProperty(String.valueOf(tens % 10)));
                    stringNumber.append(properties.getProperty(String.valueOf("teen")));
                }
            }
            else {
                if (properties.containsKey(String.valueOf(tens - ones))) {
                    stringNumber.append(properties.getProperty(String.valueOf(tens - ones)));
                }
                else {
                    stringNumber.append(properties.getProperty(String.valueOf(tens / 10)));
                    stringNumber.append(properties.getProperty("tens-suffix"));
                }
                if (ones > 0) {
                    stringNumber.append(properties.getProperty("tens-after-delimiter"));
                    stringNumber.append(properties.getProperty(String.valueOf(ones)));
                }
            }
        }
        return stringNumber.toString();
    }

    public String numberInWordsHundredPart(Integer number) {
        StringBuilder hundredNumber = new StringBuilder();
        int hundreds = number / 100;
        int tens = number % 100;
        int ones = tens % 10;
        if (hundreds > 0) {
            hundredNumber.append(properties.getProperty(String.valueOf(1)));
            hundredNumber.append(properties.getProperty("hundreds-before-delimiter"));
            hundredNumber.append(properties.getProperty("hundred"));
            if (tens == 0 && ones == 0) {
                hundredNumber.append("");
            }
            else {
                hundredNumber.append(properties.getProperty("hundreds-after-delimiter"));
            }
        }
        return hundredNumber.toString();
    }

    public String numberInWordsTens(Integer number) {
        StringBuilder resultStringNumber = new StringBuilder();
        int ones = number % 10;
        if (number >= 20) {
            if (properties.containsKey(String.valueOf(number - ones))) {
                resultStringNumber.append(properties.getProperty(String.valueOf(number - ones)));
                if (number % 10 > 0) {
                    resultStringNumber.append(properties.getProperty("tens-after-delimiter"));
                    resultStringNumber.append(properties.getProperty(String.valueOf(ones)));
                }
            }
            else {
                resultStringNumber.append(properties.getProperty(String.valueOf(number / 10)));
                resultStringNumber.append(properties.getProperty("tens-suffix"));
                if (number % 10 > 0) {
                    resultStringNumber.append(properties.getProperty("tens-after-delimiter"));
                    resultStringNumber.append(properties.getProperty(String.valueOf(ones)));
                }
            }
        }
        else {
            if (number > 10 && number < 20) {
                if (properties.containsKey(String.valueOf(number))) {
                    resultStringNumber.append(properties.getProperty(String.valueOf(number)));
                } else {
                    resultStringNumber.append(properties.getProperty(String.valueOf(number % 10)));
                    resultStringNumber.append(properties.getProperty("teen"));
                }
            }
        }
        return resultStringNumber.toString();
    }

    private void close(FileInputStream is) {
        if (is == null) {
            return;
        }
        try {
            is.close();
        }
        catch (IOException ignore) {}

    }
}
