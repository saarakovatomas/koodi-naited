"""EX12 adventures tests."""
import pytest
from EX.ex12_adventure.adventure import World, Adventurer, Monster


@pytest.fixture
def world():
    """Class World."""
    return World("Mari")


def adventurer():
    """Class Adventurer."""
    return Adventurer("Jerry", "Fighter", 10)


def monster():
    """Class Monster."""
    return Monster("Poppy", "Zombie", 10)


@pytest.fixture
def world_complex():
    """Test world complex."""
    world = World("Mari")
    adventure1 = Adventurer("Jerry", "Fighter", 10)
    adventure2 = Adventurer("Benny", "Druid", 20)
    monster1 = Monster("Tom", "Zombie", 10)
    monster2 = Monster("Bat", "Zombie", 10)
    world.add_adventurer(adventure1)
    world.add_adventurer(adventure2)
    world.add_monster(monster1)
    world.add_monster(monster2)
    return world


def test_adventurer_basic():
    """Test adventure basic."""
    hero1 = Adventurer("Pille", "Wizard", 10000)
    hero2 = Adventurer("A", "Wizard782", 50)
    hero3 = Adventurer("Ben", "Druid", -10)
    hero4 = Adventurer("Jerry", "Fighter", 10)
    hero5 = Adventurer("3421", "Paladin", 30)
    hero6 = Adventurer("444444", "iii", -11)
    hero7 = Adventurer("okidoki", "mmm", 700)

    assert str(hero1) == 'Pille, the Wizard, Power: 10, Experience: 0.'
    assert str(hero2) == 'A, the Fighter, Power: 50, Experience: 0.'
    assert str(hero3) == 'Ben, the Druid, Power: -10, Experience: 0.'
    assert str(hero4) == 'Jerry, the Fighter, Power: 10, Experience: 0.'
    assert str(hero5) == '3421, the Paladin, Power: 30, Experience: 0.'
    assert str(hero6) == '444444, the Fighter, Power: -11, Experience: 0.'
    assert str(hero7) == 'okidoki, the Fighter, Power: 10, Experience: 0.'


def test_monster_basic():
    """Test monster basic."""
    monster1 = Monster("Zombiezzz", "Animal", 20)
    monster2 = Monster("Apple", "Goblin", 120)
    monster3 = Monster("XO", "Ogre", -10)

    assert str(monster1) == "Zombiezzz of type Animal, Power: 20."
    assert str(monster2) == "Apple of type Goblin, Power: 120."
    assert str(monster3) == "XO of type Ogre, Power: -10."


def test_monster_basic_zombie():
    """Test monster basic."""
    monster1 = Monster("Rat", "Zombie", 10)
    assert str(monster1) == "Undead Rat of type Zombie, Power: 10."


def test_adventurer_add_power():
    """Test add power."""
    hero = Adventurer("Doggy", "Druid", 50)

    hero.add_power(12)
    assert str(hero) == 'Doggy, the Druid, Power: 62, Experience: 0.'

    hero.add_power(-10)
    assert str(hero) == 'Doggy, the Druid, Power: 52, Experience: 0.'

    hero.add_power(200)
    assert str(hero) == 'Doggy, the Druid, Power: 252, Experience: 0.'


def test_adventurer_add_experience():
    """Test add experience."""
    hero = Adventurer("Wix", "Fighter", 10)

    hero.add_experience(10)
    assert str(hero) == 'Wix, the Fighter, Power: 10, Experience: 10.'

    hero.add_experience(-100)
    assert str(hero) == 'Wix, the Fighter, Power: 10, Experience: 10.'

    hero.add_experience(200)
    assert str(hero) == 'Wix, the Fighter, Power: 31, Experience: 0.'


def test_world_lists():
    """World lists."""
    world = World("Jep")
    assert world.get_python_master() == "Jep"
    assert world.get_adventurer_list() == []
    assert world.get_monster_list() == []
    assert world.get_graveyard() == []


def test_add_player():
    """Add player."""
    world = World("Numps")
    hero = Adventurer("Wix", "Fighter", 10)
    monsu = Monster("Rat", "Zombie", 10)

    world.add_adventurer(hero)
    world.add_monster(monsu)

    assert world.get_adventurer_list() == [hero]
    assert world.get_monster_list() == [monsu]


def test_remove_character(world_complex):
    """Test remove character."""
    world = World("Kitty")
    hero1 = Adventurer("Pille", "Wizard", 20)
    hero2 = Adventurer("Jerry", "Fighter", 20)
    hero3 = Adventurer("Ben", "Druid", 20)

    monster1 = Monster("Zombiezzz", "Animal", 20)
    monster2 = Monster("Apple", "Goblin", 20)
    monster3 = Monster("XO", "Ogre", 20)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.get_graveyard().append(hero3)

    world.add_monster(monster1)
    world.add_monster(monster2)
    world.get_graveyard().append(monster3)

    world.remove_character("Jerry")
    world.remove_character("Apple")
    world.remove_character("Ben")

    assert world.get_adventurer_list() == [hero1]
    assert world.get_monster_list() == [monster1]
    assert world.get_graveyard() == [monster3, hero2, monster2]


def test_revive_graveyard():
    """Test revive graveyard."""
    world = World("Kity")
    hero1 = Adventurer("Pille", "Wizard", 20)
    hero2 = Adventurer("Jerry", "Fighter", 20)

    monster1 = Monster("Zombiezzz", "Zombie", 20)
    monster2 = Monster("Apple", "Goblin", 20)
    world.revive_graveyard()
    world.get_graveyard().append(hero1)
    world.get_graveyard().append(hero2)
    world.get_graveyard().append(monster1)
    world.get_graveyard().append(monster2)

    world.necromancers_active(True)
    world.revive_graveyard()
    assert world.get_graveyard() == []
    assert world.get_monster_list()[0].name == f"Undead {hero1.name}"
    assert world.get_monster_list()[0].type == f"Zombie {hero1.class_type}"
    assert world.get_monster_list()[1].name == f"Undead {hero2.name}"
    assert world.get_monster_list()[1].type == f"Zombie {hero2.class_type}"
    assert world.get_monster_list()[2].type == "Zombie"


def test_get_active():
    """Test active."""
    world = World("Jep")
    assert world.get_active_adventurers() == []
    assert world.get_active_monsters() == []


def test_strongest_adventurer():
    """Test strongest adventure."""
    world = World("Strong")
    active = []
    hero1 = Adventurer("Pille", "Fighter", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Fighter", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)

    active.append(world.add_strongest_adventurer("Fighter"))

    assert [hero4]
    assert hero4 not in world.get_adventurer_list()


def test_weakest_adventurer():
    """Test weakest adventurer."""
    world = World("Weak")
    active = []
    hero1 = Adventurer("Pille", "Fighter", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Fighter", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)

    active.append(world.add_weakest_adventurer("Fighter"))

    assert [hero2]
    assert hero2 not in world.get_adventurer_list()


def test_add_most_experienced_adventurer():
    """Test add the most experienced adventure."""
    world = World("Experience")
    active = []
    hero1 = Adventurer("Pille", "Fighter", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Fighter", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)

    hero1.add_experience(10)
    hero2.add_experience(20)
    hero3.add_experience(44)
    hero4.add_experience(77)

    active.append(world.add_most_experienced_adventurer("Fighter"))
    assert [hero4]


def test_add_least_experienced_adventurer():
    """Test add the least experienced adventure."""
    world = World("Experience")
    active = []
    hero1 = Adventurer("Pille", "Fighter", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Fighter", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)

    hero1.add_experience(10)
    hero2.add_experience(20)
    hero3.add_experience(44)
    hero4.add_experience(77)

    active.append(world.add_most_experienced_adventurer("Fighter"))
    assert [hero1]


def test_add_adventurer_by_name():
    """Add adventurer by name."""
    world = World("name")
    active = []
    hero1 = Adventurer("Pille", "Fighter", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Fighter", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)

    active.append(world.add_adventurer_by_name("Jerry"))

    assert [hero2]
    assert hero2 not in world.get_adventurer_list()


def test_add_all_adventurers_of_class_type():
    """Add all adventures of class_type."""
    world = World("class_type")
    active = []
    hero1 = Adventurer("Pille", "Paladin", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Druid", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)
    active.append(world.add_all_adventurers_of_class_type("Fighter"))

    assert [hero2, hero4]


def test_add_all_adventures():
    """Add all adventures."""
    world = World("class_type")
    active = []
    hero1 = Adventurer("Pille", "Paladin", 23)
    hero2 = Adventurer("Jerry", "Fighter", 12)
    hero3 = Adventurer("Pille87", "Druid", 30)
    hero4 = Adventurer("Jerryps", "Fighter", 50)

    world.add_adventurer(hero1)
    world.add_adventurer(hero2)
    world.add_adventurer(hero3)
    world.add_adventurer(hero4)

    for player in world.get_adventurer_list():
        active.append(player)

    assert [hero1, hero2, hero3, hero4]


def test_strongest_monster():
    """Test strongest monster."""
    world = World("Strong")
    active = []
    monster1 = Monster("Zombiezzz", "Zombie", 35)
    monster2 = Monster("Apple", "Zombie", 20)
    monster3 = Monster("Zombiezzz", "Zombie", 97)
    monster4 = Monster("Apple", "Zombie", 45)

    world.add_monster(monster1)
    world.add_monster(monster2)
    world.add_monster(monster3)
    world.add_monster(monster4)

    active.append(world.add_strongest_monster())

    assert [monster3]
    assert monster3 not in world.get_adventurer_list()


def test_weakest_monster():
    """Test weakest monster."""
    world = World("Weak")
    active_list = []
    monster1 = Monster("Zombiezzz", "Zombie", 35)
    monster2 = Monster("Apple", "Zombie", 20)
    monster3 = Monster("Zombiezzz", "Zombie", 97)
    monster4 = Monster("Apple", "Zombie", 45)

    world.add_monster(monster1)
    world.add_monster(monster2)
    world.add_monster(monster3)
    world.add_monster(monster4)

    active_list.append(world.add_weakest_monster())

    assert [monster2]
    assert monster2 not in world.get_adventurer_list()


def test_add_monster_by_name():
    """Add monster by name."""
    world = World("name")
    active_list = []
    monster1 = Monster("Zombiezzz", "Animal", 35)
    monster2 = Monster("Apple", "Zombie", 20)
    monster3 = Monster("Zombiezzz", "Animal", 97)
    monster4 = Monster("Apple", "Org", 45)

    world.add_monster(monster1)
    world.add_monster(monster2)
    world.add_monster(monster3)
    world.add_monster(monster4)
    active_list.append(world.add_monster_by_name("Apple"))
    assert [monster2]


def test_add_all_monsters_of_type():
    """Add all monsters of type."""
    world = World("Type")
    active_list = []
    monster1 = Monster("Zombiezzz", "Animal", 35)
    monster2 = Monster("Apple", "Zombie", 20)
    monster3 = Monster("Zombiezzz", "Animal", 97)
    monster4 = Monster("Apple", "Org", 45)

    world.add_monster(monster1)
    world.add_monster(monster2)
    world.add_monster(monster3)
    world.add_monster(monster4)

    active_list.append(world.add_all_monsters_of_type("Animal"))
    assert [monster1, monster3]


def test_add_all_monster():
    """Add all monsters."""
    world = World("all")
    active_list = []
    monster1 = Monster("Zombiezzz", "Animal", 35)
    monster2 = Monster("Apple", "Zombie", 20)
    monster3 = Monster("Zombiezzz", "Animal", 97)
    monster4 = Monster("Apple", "Org", 45)

    world.add_monster(monster1)
    world.add_monster(monster2)
    world.add_monster(monster3)
    world.add_monster(monster4)

    for monster in world.get_monster_list():
        active_list.append(monster)
    assert [monster1, monster2, monster3, monster4]


def test_deadly():
    """Test."""
    world = World("deadly")
    hero = Adventurer("Jerry", "Paladin", 20)
    monsu = Monster("Apple", "Zombie", 11)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Paladin")

    world.go_adventure(True)

    assert world.get_graveyard()[0] == monsu
    assert world.get_adventurer_list()[0] == hero


def test_paladin_vs_zombie():
    """Test paladin vs zombie."""
    world = World("xd")
    hero = Adventurer("Jerry", "Paladin", 50)
    monsu = Monster("Apple", "Zombie", 95)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Paladin")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(True)

    assert world.get_adventurer_list()[0] == hero
    assert world.get_graveyard()[0] == monsu


def test_animal_druid_ent():
    """Test animal and druid and ent."""
    world = World("xd")
    hero = Adventurer("Jerry", "Druid", 50)
    monsu = Monster("Apple", "Animal", 95)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Druid")

    assert world.get_monster_list() == []
    assert world.get_active_adventurers() == [hero]


def test_monster_victory_false():
    """Test monster victory and dealdy is false."""
    world = World("xd")
    hero = Adventurer("Jerry", "Wizard", 50)
    monsu = Monster("Apple", "Animal", 95)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Wizard")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(False)

    assert world.get_monster_list() == [monsu]
    assert world.get_active_monsters() == []
    assert world.get_adventurer_list() == [hero]
    assert world.get_active_adventurers() == []


def test_monster_victory_true():
    """Test monster victory and deadly is true."""
    world = World("xd")
    hero = Adventurer("Jerry", "Wizard", 50)
    monsu = Monster("Apple", "Animal", 95)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Wizard")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(True)

    assert world.get_monster_list() == [monsu]
    assert world.get_active_monsters() == []
    assert world.get_graveyard() == [hero]
    assert world.get_active_adventurers() == []


def test_adventure_victory_false():
    """Test monster victory and dealdy is false."""
    world = World("xd")
    hero = Adventurer("Jerry", "Wizard", 96)
    monsu = Monster("Apple", "Animal", 95)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Wizard")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(False)

    assert world.get_monster_list() == [monsu]
    assert world.get_active_monsters() == []
    assert world.get_adventurer_list() == [hero]
    assert world.get_active_adventurers() == []


def test_adventure_victory_true():
    """Test monster victory and dealdy is true."""
    world = World("xd")
    hero = Adventurer("Jerry", "Wizard", 96)
    monsu = Monster("Apple", "Animal", 95)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Wizard")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(True)

    assert world.get_graveyard() == [monsu]
    assert world.get_active_monsters() == []
    assert world.get_adventurer_list() == [hero]
    assert world.get_active_adventurers() == []


def test_adventure_and_monster_victory_equal_true():
    """Test monster victory and dealdy is true."""
    world = World("xd")
    hero = Adventurer("Jerry", "Wizard", 96)
    monsu = Monster("Apple", "Animal", 96)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Wizard")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(True)

    assert world.get_monster_list() == [monsu]
    assert world.get_active_monsters() == []
    assert world.get_adventurer_list() == [hero]
    assert world.get_active_adventurers() == []


def test_adventure_and_monster_victory_equal_false():
    """Test monster victory and dealdy is true."""
    world = World("xd")
    hero = Adventurer("Jerry", "Wizard", 96)
    monsu = Monster("Apple", "Animal", 96)

    world.add_adventurer(hero)
    world.add_monster(monsu)
    world.add_strongest_monster()
    world.add_strongest_adventurer("Wizard")

    assert world.get_active_adventurers() == [hero]
    assert world.get_active_monsters() == [monsu]

    world.go_adventure(False)

    assert world.get_monster_list() == [monsu]
    assert world.get_active_monsters() == []
    assert world.get_adventurer_list() == [hero]
    assert world.get_active_adventurers() == []
