"""EX12 Dungeons and Pythons."""
import math


class Adventurer:
    """Adventure."""

    def __init__(self, name: str, class_type: str, power: int, experience: int = 0):
        """Adventure."""
        self.name = name

        if class_type == "Druid" or class_type == "Wizard" or class_type == "Paladin":
            self.class_type = class_type
        else:
            self.class_type = "Fighter"

        if power > 99:
            self.power = 10
        else:
            self.power = power

        if experience <= 0:
            self.experience = 0
        else:
            self.experience = experience

        self.is_active = False

    def __repr__(self):
        """Adventures."""
        return f"{self.name}, the {self.class_type}, Power: {self.power}, Experience: {self.experience}."

    def add_power(self, power: int):
        """Adventure."""
        self.power += power

    def add_experience(self, exp: int):
        """Adventure."""
        if exp > 0:
            self.experience += exp

        if self.experience > 99:
            self.power += math.floor(self.experience / 10)
            self.experience = 0


class Monster:
    """Adventures."""

    def __init__(self, name: str, type: str, power: int):
        """Adventure."""
        self.type = type
        self.power = power
        self.is_active = False
        self.original_name = name

    @property
    def name(self):
        """Get name."""
        if "Zombie" in self.type:
            return "Undead " + self.original_name
        return self.original_name

    def __repr__(self):
        """Adventure."""
        return f"{self.name} of type {self.type}, Power: {self.power}."


class World:
    """Adventure."""

    def __init__(self, python_master: str):
        """Adventure."""
        self.python_master = python_master
        self.adventurer_list = []
        self.active_adventurer_list = []
        self.monster_list = []
        self.active_monster_list = []
        self.graveyard = []
        self.necromancers_activ = False

    def get_python_master(self):
        """Adventure."""
        return self.python_master

    def get_graveyard(self):
        """Adventure."""
        return self.graveyard

    def get_adventurer_list(self):
        """Adventure."""
        return self.adventurer_list

    def get_monster_list(self):
        """Adventure."""
        # return self.active_monster_list.append(filter(lambda x: not x.is_active, self.monster_list))
        return self.monster_list

    def add_adventurer(self, player):
        """Adventure."""
        if isinstance(player, Adventurer):
            self.adventurer_list.append(player)

    def add_monster(self, monsu):
        """Adventure."""
        if isinstance(monsu, Monster):
            self.monster_list.append(monsu)

    def remove_character(self, name: str):
        """Put character to graveyard."""
        for adventurer in self.adventurer_list:
            if adventurer.name == name:
                self.adventurer_list.remove(adventurer)
                self.graveyard.append(adventurer)
                return

        for monster in self.monster_list:
            if monster.name == name:
                self.monster_list.remove(monster)
                self.graveyard.append(monster)
                return

        for player in self.graveyard:
            if player.name == name:
                self.graveyard.remove(player)

    def necromancers_active(self, active: bool):
        """Adventures."""
        self.necromancers_activ = active

    def revive_graveyard(self):
        """Adventures."""
        if self.necromancers_activ:
            for player in self.graveyard:
                if isinstance(player, Monster):
                    player.type = "Zombie"
                    self.monster_list.append(player)
                if isinstance(player, Adventurer):
                    player.type = "Zombie"
                    self.monster_list.append(Monster(player.name, f"Zombie {player.class_type}", player.power))
            self.graveyard = []
            self.necromancers_active(False)

    def get_active_adventurers(self):
        """Get active adventurers."""
        return self.active_adventurer_list

    def add_strongest_adventurer(self, class_type: str):
        """Add strongest adventurer."""
        # get the adventurer with the biggest power number, save it in a variable
        # remove adventurer from self.adventurer list
        # add into active adventurer list
        biggest_power = list(filter(lambda x: x.class_type == class_type, self.adventurer_list))
        biggest_power.sort(key=lambda x: -x.power)
        if biggest_power:
            self.active_adventurer_list.append(biggest_power[0])
            self.adventurer_list.remove(biggest_power[0])

    def add_weakest_adventurer(self, class_type: str):
        """Add weakest."""
        # get the monster with the lowest power number, save it in a variable
        # remove monster from self.monster list
        # add into active monster list
        weakest_power = list(filter(lambda x: x.class_type == class_type, self.adventurer_list))
        weakest_power.sort(key=lambda x: x.power)
        if weakest_power:
            self.active_adventurer_list.append(weakest_power[0])
            self.adventurer_list.remove(weakest_power[0])

    def add_most_experienced_adventurer(self, class_type: str):
        """Add most experienced adventurer."""
        most_experienced = list(filter(lambda x: x.class_type == class_type, self.adventurer_list))  # get the adventurer with the most experience points, save it in a variable
        most_experienced.sort(key=lambda x: -x.experience)
        if most_experienced:
            self.active_adventurer_list.append(most_experienced[0])  # add into active adventurer list
            self.adventurer_list.remove(most_experienced[0])  # remove adventurer from self.adventurer list

    def add_least_experienced_adventurer(self, class_type: str):
        """Add least experienced adventurer."""
        # get the adventurer with the least experience points, save it in a variable
        # remove adventurer from self.adventurer list
        # add into active adventurer list
        least_experienced = list(filter(lambda x: x.class_type == class_type, self.adventurer_list))
        least_experienced.sort(key=lambda x: x.experience)
        if least_experienced:
            self.active_adventurer_list.append(least_experienced[0])
            self.adventurer_list.remove(least_experienced[0])

    def add_adventurer_by_name(self, name: str):
        """Add adventurer by name."""
        # get the adventurer with the specific name, if they are not active already, save it in a variable
        # remove adventurer from self.adventurer list
        # add into active adventurer list
        by_name = list(filter(lambda x: x.name == name, self.adventurer_list))
        if by_name:
            self.active_adventurer_list.append(by_name[0])
            if by_name[0] in self.adventurer_list:
                self.adventurer_list.remove(by_name[0])

    def add_all_adventurers_of_class_type(self, class_type: str):
        """Adventures."""
        # get all adventurers with specific class type, from self.adventurer list(those who are not active), save in list
        # remove adventurers from self.adventurer list, add into active adventurers list in 1 for loop or addall
        of_class_type = list(filter(lambda x: x.class_type == class_type, self.adventurer_list))
        of_class_type.sort(key=lambda x: x.experience)
        if of_class_type:
            for x in of_class_type:
                self.active_adventurer_list.append(x)

        if of_class_type in self.adventurer_list:
            self.adventurer_list.remove(of_class_type)

    def add_all_adventurers(self):
        """Adventures."""
        for player in self.adventurer_list:
            self.active_adventurer_list.append(player)
        self.adventurer_list = []

    def add_strongest_monster(self):
        """Adventure."""
        strongest_power = sorted(self.monster_list, key=lambda x: -x.power)
        if strongest_power:
            self.active_monster_list.append(strongest_power[0])
            if strongest_power[0] in self.monster_list:
                self.monster_list.remove(strongest_power[0])

    def get_active_monsters(self):
        """Adventure."""
        return self.active_monster_list

    def add_monster_by_name(self, name: str):
        """Adventures."""
        by_name = list(filter(lambda x: x.name == name, self.monster_list))
        if by_name:
            self.active_monster_list.append(by_name[0])
            if by_name[0] in self.monster_list:
                self.monster_list.remove(by_name[0])

    def add_weakest_monster(self):
        """Adventures."""
        weakest_power = sorted(self.monster_list, key=lambda x: x.power)
        if weakest_power:
            self.active_monster_list.append(weakest_power[0])
            self.monster_list.remove(weakest_power[0])

    def add_all_monsters_of_type(self, type: str):
        """Adventures."""
        of_class_type = list(filter(lambda x: x.type == type, self.monster_list))
        if of_class_type:
            for x in of_class_type:
                self.active_monster_list.append(x)

        if of_class_type in self.monster_list:
            self.monster_list.remove(of_class_type)

    def add_all_monsters(self):
        """Adventures."""
        for player in self.monster_list:
            self.active_monster_list.append(player)
        self.monster_list = []

    def go_adventure(self, deadly: bool = False):
        """Adventure."""
        adventurer_power_total = 0
        monster_power_total = 0
        temp_list = []
        paladin_power_up = ["Zombie", "Zombie Fighter", "Zombie Druid", "Zombie Paladin", "Zombie Wizard"]
        druid = list(filter(lambda x: "Druid" == x.class_type, self.active_adventurer_list))
        pala_monster = list(filter(lambda x: x.type in paladin_power_up, self.active_monster_list))
        for adventurer in self.active_adventurer_list:
            if pala_monster and adventurer.class_type == "Paladin":
                adventurer_power_total += adventurer.power * 2
            else:
                adventurer_power_total += adventurer.power
        for monster in self.active_monster_list:
            if druid and (monster.type == "Animal" or monster.type == "Ent"):
                self.monster_list.append(monster)
                temp_list.append(monster)
            else:
                monster_power_total += monster.power
        for monsu in temp_list:
            self.active_monster_list.remove(monsu)
        self.go_adventure2(deadly, monster_power_total, adventurer_power_total)

    def go_adventure2(self, deadly, monste, adventure):
        """Vol2."""
        exp = math.floor(monste / len(self.active_adventurer_list))
        if adventure > monste:
            for adventurer in self.active_adventurer_list:
                if deadly is True:
                    adventurer.add_experience(math.floor(exp * 2))
                    self.graveyard += self.active_monster_list
                    self.active_monster_list = []
                    self.adventurer_list += self.active_adventurer_list
                    self.active_adventurer_list = []
                else:
                    adventurer.add_experience(exp)
                    self.adventurer_list += self.active_adventurer_list
                    self.active_adventurer_list = []
                    self.monster_list += self.active_monster_list
                    self.active_monster_list = []

        elif adventure == monste:
            for adventurer in self.active_adventurer_list:
                adventurer.add_experience(math.floor(exp / 2))
                self.adventurer_list += self.active_adventurer_list
                self.active_adventurer_list = []
                self.monster_list += self.active_monster_list
                self.active_monster_list = []
        else:
            if deadly is True:
                self.graveyard += self.active_adventurer_list
                self.active_adventurer_list = []
                self.monster_list += self.active_monster_list
                self.active_monster_list = []
            self.adventurer_list += self.active_adventurer_list
            self.active_adventurer_list = []
            self.monster_list += self.active_monster_list
            self.active_monster_list = []
