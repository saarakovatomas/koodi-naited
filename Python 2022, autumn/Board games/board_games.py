"""Board games."""


class Statistics:
    """Board games statistics."""

    def __init__(self, filename):
        """Statistics."""
        self.games_data = []
        #  self.file.close()
        self.player_names = []
        self.game_names = []
        self.games = []

        self.file = open(filename, "r")
        for line in self.file:
            self.games_data.append(line.strip().split(';'))
        # for info in self.games_data:
        #     names = info[1].split(";")
        #     print(names)
        print(self.games_data)

    def get_player_names(self):
        """Get player names."""
        for info in self.games_data:
            names = info[1].split(',')
            for name in names:
                if name not in self.player_names:
                    self.player_names.append(name)
        return self.player_names

    def get_game_names(self):
        """Get game names."""
        for info in self.games_data:
            games = info[0]
            if games not in self.game_names:
                self.game_names.append(games)
        return self.game_names

    def get_games_played_amount(self):
        """Get games played amount."""
        for info in self.games_data:
            game = info[0]
            self.games.append(game)
        return len(self.games)

    def get_games_played_of_type(self, result_type):
        """Get games played of type."""
        res = ["points", "places", "winner"]
        total = 0
        for info in self.games_data:
            #print(info[2])
            res_type = info[2]
            if result_type in res and result_type == res_type:
                total += info.count(result_type)
        return total

    def get_games_amount_played_by(self, player_name: str):
        """Get games amount played by."""
        total = 0
        for info in self.games_data:
            names = info[1].split(',')
            #print(names)
            if player_name in names:
               total += names.count(player_name)
        return total

    def get_favourite_game(self, player_name: str):
        """Get favorite game."""
        listy = []
        for info in self.games_data:
            names = info[1].split(",")
            if player_name in names:
                listy.append(info[0])
        return listy

    def get_amount_of_games_won(self, player_name: str):
        """Get amount of games won."""
        total = 0
        for info in self.games_data:
            names = info[1].split(',')
            if player_name in names and info[2] == "places":
                places = info[3].split(",")
                if places[0] == player_name:
                    total = total + 1
            if player_name in names and info[2] == "winner":
                if info[3] == player_name:
                    total = total + 1
            else:
                for i, n in enumerate(names):
                    points = info[3].split(",")
                    #print(points)
                    if n == player_name and points[i] == max(points):
                        total = total + 1
        return total

    def get_games_played_of_name(self, game_name: str):
        """Get games played of name."""
        total = 0
        for info in self.games_data:
            games = info[0]
            if games == game_name:
                total += 1
        return total

    def get_amount_of_players_most_often_played_with(self, game_name: str):
        """Get amount of players most often played with."""
        for info in self.games_data:
            names = info[1].split(",")
            if game_name in info:
                return len(names)

    def get_player_with_most_amount_of_wins(self, game_name: str):
        """Get player with most amount of wins."""
        for info in self.games_data:
            if game_name in info and "points" in info[2]:
                points = info[3].split(",")
                names = info[1].split(",")
                for x, i in enumerate(points):
                    if max(i):
                        return names[x]
            if game_name in info and "winner" in info[2]:
                return info[3]
            if game_name in info and "places" in info:
                winner = info[3].split(",")
                return winner[0]

    def get_most_frequent_winner(self, game_name: str):
        """Get most frequent winner."""
        pass

    def get_player_with_most_amount_of_losses(self, game_name: str):
        """Get player with most amount of losses."""
        for info in self.games_data:
            point = list(filter(lambda x: "points", info))
            place = list(filter(lambda x: "places", info))
            names = info[1].split(",")
            heheh = info[3].split(",")
            if game_name in info and point:
                index = heheh.index(min(heheh))
                return names[index]
            if game_name in info and place:
                return heheh[1]

    def get_most_frequent_loser(self, game_name: str):
        """Get most frewquent loser."""
        pass

    def get_record_holder(self, game_name: str):
        """Get record holder."""
        pass


if __name__ == '__main__':
    filename = "data"
    d = Statistics(filename)
    print(d.get_player_names())
    print(d.get_game_names())
    print(d.get_games_played_amount())
    print(d.get_games_played_of_type("points"))
    print(d.get_games_amount_played_by("joosep"))
    print(d.get_favourite_game("kristjan"))
    print(d.get_amount_of_games_won("jaak"))
    print(d.get_amount_of_games_won("riho"))
    print(d.get_amount_of_games_won("joosep"))
    print(d.get_player_with_most_amount_of_wins("chess"))
    print(d.get_most_frequent_winner("chess"))
    print(d.get_player_with_most_amount_of_losses("7 wonders"))
    print(d.get_player_with_most_amount_of_losses("chess"))

