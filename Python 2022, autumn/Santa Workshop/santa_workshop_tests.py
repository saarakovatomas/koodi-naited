"""Santa tests"""
import pytest
from EX.ex15_santas_workshop.santas_workshop import Child, Child_list, Product, Warehouse


def child():
    """Class Child."""
    return Child("Libby", "United Kingdom")


def child_list():
    """Class ChildList."""
    return Child_list()


def product():
    """Class Product."""
    return Product("Swimming flippers", 25, 3, 1000)


@pytest.fixture
def warehouse():
    """Class Warehouse."""
    return Warehouse()


def test_child_basics():
    """Test child_list basics."""
    child1 = Child("Libby", "United Kingdom")
    child2 = Child("Keira", "Germany")
    child3 = Child("Jodie", "Sweden")

    assert str(child1) == "[Libby, United Kingdom, []]"
    assert str(child2) == "[Keira, Germany, []]"
    assert str(child3) == "[Jodie, Sweden, []]"


def test_product_basics():
    """Test Product basics."""
    product1 = Product("Swimming flippers", 25, 3, 1000)
    product2 = Product("Polar bear plushie", 15, 2, 200)
    product3 = Product("Wireless headphones", 89, 5, 540)

    assert str(product1) == "Product name is Swimming flippers, price is 25, time is 3, weight is 1000 g."
    assert str(product2) == "Product name is Polar bear plushie, price is 15, time is 2, weight is 200 g."
    assert str(product3) == "Product name is Wireless headphones, price is 89, time is 5, weight is 540 g."


def test_get_nice_children():
    """Test get nice children."""
    child_listy = Child_list()
    assert str(child_listy.get_nice_children_list()[0]) == "[Libby, United Kingdom, []]"
    assert len(child_listy.get_nice_children_list()) == 582


def test_get_naughty_children():
    """Test get nice children."""
    child_listy = Child_list()
    assert str(child_listy.get_naughty_children_list()[0]) == "[Tanya, United Kingdom, []]"
    assert len(child_listy.get_naughty_children_list()) == 216


def test_add_wish_list():
    """Test add wishlist."""
    child_listy = Child_list()
    child_listy.get_nice_children_list()
    #child_listy.add_wishlist()
    assert str(child_listy.add_wishlist()[0]) == "[Libby, United Kingdom, ['Zebra Jumpy', 'Princess dress', 'Lego death star']]"


def test_get_products_from_factory(warehouse):
    """Test get products from factory."""
    # warehouse.get_products_from_factory('Swimming flippers')
    assert str(warehouse.get_products_from_factory('Swimming flippers')) == "{'Swimming flippers': [Product name is Swimming flippers, price is 25, time is 3, weight is 1000 g.]}"


def test_get_product(warehouse):
    """Get product."""
    assert warehouse.get_product("Zebra Jumpy") == "Zebra Jumpy"
    assert warehouse.get_product("Princess dress") == "Princess dress"


def test_get_and_add_product_inventory(warehouse):
    """Get and add product to inventory."""
    assert warehouse.get_and_add_product_inventory("Princess dress") == {'Princess dress': 1}
