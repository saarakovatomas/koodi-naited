"""Santas workshop."""
import csv
import urllib.request
import json
import urllib.parse
import urllib.error


class Child:
    """Class child."""

    def __init__(self, name, country):
        """Ok."""
        self.name = name
        self.country = country
        self.wishlist = []

    def __repr__(self):
        """Okk."""
        return f'[{self.name}, {self.country}, {self.wishlist}]'


class Child_list:
    """Class child list."""

    def __init__(self):
        """ok."""
        self.nice_children = []
        self.naughty_children = []
        self.wishs = []

    def get_nice_children_list(self):
        """Add nice children list."""
        with open("nice_list.csv") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                child = Child(row[0], row[1][1:])
                self.nice_children.append(child)
        return self.nice_children

    def get_naughty_children_list(self):
        """Add naughty children list."""
        with open("naughty_list.csv") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                child = Child(row[0], row[1][1:])
                self.naughty_children.append(child)
        return self.naughty_children

    def add_wishlist(self):
        """Get normal childer."""
        with open("wish_list.csv") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                temporary_wishlist = []
                for wish in row[1:]:
                    if wish.endswith(" "):
                        temporary_wishlist.append(wish[1:-1])
                    else:
                        temporary_wishlist.append(wish[1:])

                for child in self.nice_children:
                    if child.name == row[0]:
                        child.wishlist += temporary_wishlist
        return self.nice_children


class Product:
    """Api."""

    def __init__(self, name, price, production_time, weight):
        """OK."""
        self.name = name
        self.price = price
        self.production_time = production_time
        self.weight = weight

    def __repr__(self):
        """Ok."""
        return f'Product name is {self.name}, price is {self.price}, time is {self.production_time}, weight is {self.weight} g.'


class Warehouse:
    """Warehouse."""
    def __init__(self):
        self.products = {}
        self.products_inventory = {}
        self.wishes_for_christmas = []

    def get_products_from_factory(self, name: str) -> Product:
        """Get product."""
        url = "https://cs.ttu.ee/services/xmas/gift?name="
        product_name = urllib.parse.quote_plus(name)
        try:
            with urllib.request.urlopen(url + product_name) as f:
                contents = f.read()
                data = json.loads(contents.decode('utf-8'))
                product = Product(data["gift"], data["material_cost"], data["production_time"], data["weight_in_grams"])
                if data["gift"] not in self.products:
                    self.products[data["gift"]] = []
                self.products[data["gift"]].append(product)
            return self.products
        except urllib.error.HTTPError:
            return None

    def get_product(self, name: str):
        """Get product."""
        url = "https://cs.ttu.ee/services/xmas/gift?name="
        product_name = urllib.parse.quote_plus(name)
        with urllib.request.urlopen(url + product_name) as f:
            contents = f.read()
            data = json.loads(contents.decode('utf-8'))
            print(data)
            for value in data.values():
                if data['gift'] == name:
                    return value

    def get_and_add_product_inventory(self, name):
        """Get inventory."""
        url = "https://cs.ttu.ee/services/xmas/gift?name="
        product_name = urllib.parse.quote_plus(name)
        with urllib.request.urlopen(url + product_name) as f:
            contents = f.read()
            data = json.loads(contents.decode('utf-8'))
            if data["gift"] not in self.products_inventory:
                self.products_inventory[data["gift"]] = 1
            else:
                self.products_inventory[data["gift"]] += 1
            return self.products_inventory
