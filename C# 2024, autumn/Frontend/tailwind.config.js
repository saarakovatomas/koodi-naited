/** @type {import('tailwindcss').Config} */
// tailwind.config.js
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        customHover: '#fffff', // Custom color for hover state
        customFocus: '#fffff', // Custom color for focus state
        backgroundFocus: '#fffff', // Optional background color for focused input
        customToggle: '#9D8C8B',
      },
    },
  },
  plugins: [],
};
