import { defineStore } from "pinia";
import { ref } from "vue";
import { useApi } from "~/composables/useApi";
import type { Diary } from "~/types/diary";

export const useDiaryStore = defineStore('diary', () => {
  const api = useApi();
  const diaries = ref<Diary[]>([]);

  // Load all diaries from the server
  const loadDiary = async () => {
    const response = await api.customFetch<Diary[]>("Diaries");
    diaries.value = response;
  };

  const loadDiaryById = async (diaryId: number): Promise<Diary | undefined> => {
    // Check if diary is already in the store
    let diary = diaries.value.find(d => d.id === diaryId);
    
    if (!diary) {
      // Fetch the diary from the server if not found locally
      diary = await api.customFetch<Diary>(`Diaries/${diaryId}`);
      
      if (diary) {
        diaries.value.push(diary);  // Cache the diary in the store
      }
    }
    return diary;
  };

  // Add a new diary
  const addDiary = async (diary: Omit<Diary, 'id'>) => {
    const response = await api.customFetch<Diary>("Diaries", {
      method: "POST",
      body: diary,
    });

    if (response && response.id) {
      diaries.value.push(response);  // Add the new diary to the store
    }
  };

  // Update an existing diary
  const updateDiary = async (diary: Diary) => {
    const response = await api.customFetch<Diary>(`Diaries/${diary.id}`, {
      method: "PUT",
      body: diary,
    });

    if (response && response.id) {
      // Find the index of the diary in the store and update it
      const index = diaries.value.findIndex(d => d.id === diary.id);
      if (index > -1) {
        diaries.value[index] = response;  // Update the diary in the store
      }
    }
  };

  // Remove a diary
  const removeDiary = async (diary: Diary) => {
    await api.customFetch(`Diaries/${diary.id}`, {
      method: "DELETE",
    });

    const index = diaries.value.findIndex(d => d.id === diary.id);
    if (index > -1) {
      diaries.value.splice(index, 1);  // Remove the diary from the store
    }
  };

  // Share a diary with a user
  const shareDiaryWithUser = (diaryId: number, email: string) => {
    const diary = diaries.value.find(d => d.id === diaryId);
    if (diary && !diary.sharedWith.includes(email)) {
      diary.sharedWith.push(email);  // Add the email to the shared list
    }
  };


  return { diaries, addDiary, updateDiary, removeDiary, shareDiaryWithUser, loadDiary, loadDiaryById };
});
