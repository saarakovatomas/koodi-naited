import { defineStore } from "pinia";
import { ref, computed } from "vue";
import { useAuth } from "~/composables/useAuth";
import type { Countdown } from "~/types/countdown";

export const useCountdownStore = defineStore("countdown", () => {
  const api = useApi();
  const countdowns = ref<Countdown[]>([]);
  const activeCountdown = ref<Countdown | null>(null); // Salvestab aktiivse loenduri

  // Laadib kõik loendurid serverist
  const loadCountdowns = async () => {
    const response = await api.customFetch<Countdown[]>("Countdowns");
    countdowns.value = response;
  };

  // Laadib ühe loenduri ID alusel
  const loadCountdownById = async (countdownId: number): Promise<Countdown | undefined> => {
    let countdown = countdowns.value.find(c => c.id === countdownId);
    
    if (!countdown) {
      countdown = await api.customFetch<Countdown>(`Countdowns/${countdownId}`);
      
      if (countdown) {
        countdowns.value.push(countdown);  // Salvestab loenduri store’i
      }
    }
    return countdown;
  };

  // Laadib aktiivse loenduri (näiteks see, mille `isActive` on true)
  const loadActiveCountdown = async () => {
    const response = await api.customFetch<Countdown[]>("Countdowns");
    const active = response.find(c => c.isActive); // Leia loendur, mille `isActive` on true
    if (active) {
      activeCountdown.value = active;
    }
  };

  // Loob uue loenduri
  const addCountdown = async (countdown: Omit<Countdown, 'id'>) => {
    const response = await api.customFetch<Countdown>("Countdowns", {
      method: "POST",
      body: countdown,
    });
    
    if (response && response.id) {
      countdowns.value.push(response);  // Lisa uus loendur store'i
      if (response.isActive) {
        activeCountdown.value = response; // Määrab aktiivseks, kui uus loendur on `isActive: true`
      }
    }
  };

  // Uuendab olemasolevat loendurit
  const updateCountdown = async (countdown: Countdown) => {
    const response = await api.customFetch<Countdown>(`Countdowns/${countdown.id}`, {
      method: "PUT",
      body: countdown,
    });

    if (response && response.id) {
      const index = countdowns.value.findIndex(c => c.id === countdown.id);
      if (index > -1) {
        countdowns.value[index] = response;  // Uuendab loendurit store’is
        if (response.isActive) {
          activeCountdown.value = response; // Uuendab aktiivse loenduri
        } else if (activeCountdown.value?.id === countdown.id) {
          activeCountdown.value = null; // Nullib aktiivse loenduri, kui uuendus inaktiveerib selle
        }
      }
    }
  };

  // Kustutab loenduri
  const removeCountdown = async (countdown: Countdown) => {
    await api.customFetch(`Countdowns/${countdown.id}`, {
      method: "DELETE",
    });

    const index = countdowns.value.findIndex(c => c.id === countdown.id);
    if (index > -1) {
      countdowns.value.splice(index, 1);  // Eemaldab loenduri store’ist
      if (activeCountdown.value?.id === countdown.id) {
        activeCountdown.value = null; // Nullib aktiivse loenduri, kui see kustutatakse
      }
    }
  };

  return { countdowns, activeCountdown, addCountdown, updateCountdown, removeCountdown, loadCountdowns, loadCountdownById, loadActiveCountdown };
});
