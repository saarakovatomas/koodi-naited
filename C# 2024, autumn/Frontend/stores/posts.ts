import { defineStore } from "pinia";
import { ref } from "vue";
import type { Post } from "~/types/post";
import { useApi } from "~/composables/useApi";
import { usePhotoStore } from "~/stores/photos";
import { useAuth } from "~/composables/useAuth";

export const usePostStore = defineStore('post', () => {
  const api = useApi();
  const photosStore = usePhotoStore();
  const posts = ref<Post[]>([]); // Kõik postitused

  // Laadib kõik postitused
  const loadPosts = async () => {
    const response = await api.customFetch<Post[]>("Posts");
    console.log(response); // Kuva postituste struktuur
    posts.value = response;
  };

  // Laadib postitused konkreetse päeviku (diaryId) põhjal
  const loadPostsByDiaryId = async (diaryId: number) => {
    const response = await api.customFetch<Post[]>(`Posts?diaryId=${diaryId}`);
    posts.value = response;
  };

  // Lisab uue postituse kindlale päevikule
  const addPost = async (post: Omit<Post, 'id'>): Promise<Post | null> => {
    try {
      const response = await api.customFetch<Post>("Posts", {
        method: "POST",
        body: post,
      });
      if (response) {
        // Add the new post to the posts list to update the view immediately
        posts.value.push(response);
        return response;
      } else {
        console.error("Failed to add the post. Response was null.");
        return null;
      }
    } catch (error) {
      console.error("Error adding post:", error);
      return null;
    }
  };
  

  // Tagastab postitused vastavalt päeviku ID-le (kohalik filter)
  const getPostsByDiaryId = (diaryId: number) => {
    return posts.value.filter((post: Post )=> post.diaryId === diaryId);
  };

  const updatePost = async (post: Post) => {
    try {
      const response = await api.customFetch<Post>(`Posts/${post.id}`, {
        method: "PUT",
        body: post, // Saadetakse kogu uuendatud postituse objekt
      });
      if (response && response.id) {
        const index = posts.value.findIndex((p) => p.id === post.id);
        if (index > -1) {
          // Uuenda ainult vajalik postitus
          posts.value[index] = response;
  
          // Sordi uuesti `pageNumber` alusel
          posts.value = [...posts.value].sort((a, b) => (a.pageNumber || 0) - (b.pageNumber || 0));
        }
      }
    } catch (error) {
      console.error("Postituse uuendamine ebaõnnestus:", error);
    }
  };
  
  
  
  

  // Postituse eemaldamine
  const removePost = async (post: Post) => {
    await api.customFetch(`Posts/${post.id}`, {
      method: "DELETE",
    });
    const index = posts.value.findIndex(p => p.id === post.id);
    if (index > -1) {
      posts.value.splice(index, 1);
    }
  };

  return {
    posts,
    loadPosts,
    loadPostsByDiaryId,
    addPost,
    getPostsByDiaryId,
    removePost,
    updatePost,
  };
});