import { defineStore } from 'pinia';
import { ref } from 'vue';
import type { Style } from '~/types/style';
import { useApi } from '~/composables/useApi'; // Update with the correct path to your `useApi`
import { useAuth } from '~/composables/useAuth';

export const useStyleStore = defineStore('style', () => {
  const api = useApi();
// API kasutamine
  const styles = ref<Style[]>([]); 

  const loadStyles = async () => {
    const response = await api.customFetch<Style[]>("Styles");
    console.log(response); // Kuva postituste struktuur
    styles.value = response;
  };

  const loadStyleByDiaryId = async (diaryId: number) => {
    const response = await api.customFetch<Style[]>(`Styles?diaryId=${diaryId}`);
    styles.value = response;
  };

  const addStyle = async (style: Omit<Style, 'id'>): Promise<Style | null> => {
    try {
      const response = await api.customFetch<Style>("Styles", {
        method: "POST",
        body: style,
      });
      if (response) {
        styles.value.push(response);
        return response;
      } else {
        console.error("Failed to add the style. Response was null.");
        return null;
      }
    } catch (error) {
      console.error("Error adding style:", error);
      return null;
    }
  };
  
  const getStyleByDiaryId = (diaryId: number) => {
    return styles.value.filter((style: Style )=> style.diaryId === diaryId);
  };

  const updateStyle = async (style: Style) => {
    try {
      const response = await api.customFetch<Style>(`Styles/${style.id}`, {
        method: "PUT",
        body: style,
      });
  
      if (response && response.id) {
        const index = styles.value.findIndex((p) => p.id === style.id);
        if (index > -1) {
          styles.value[index] = response; // Uuenda stiil store'is
        }
      }
    } catch (error) {
      console.error("Error updating style:", error);
    }
  };
  
  
  const removeStyle = async (style: Style) => {
    await api.customFetch(`Styles/${style.id}`, {
      method: "DELETE",
    });
    const index = styles.value.findIndex(p => p.id === style.id);
    if (index > -1) {
      styles.value.splice(index, 1);
    }
  };

  return {
    styles,
    loadStyles,
    loadStyleByDiaryId,
    addStyle,
    getStyleByDiaryId,
    removeStyle,
    updateStyle
  };
});
