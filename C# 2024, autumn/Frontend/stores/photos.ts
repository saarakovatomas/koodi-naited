import { defineStore } from "pinia";
import { ref } from "vue";
import type { Photo } from "~/types/Photo";
import { useApi } from "~/composables/useApi";
import { useAuth } from "~/composables/useAuth";

export const usePhotoStore = defineStore("photo", () => {
  const api = useApi();
  const photos = ref<Photo[]>([]);

  // Laadib pildid serverist
  const loadPhotos = async () => {
    try {
      const response = await api.customFetch<Photo[]>("/api/photos"); // Endpoint, mis tagastab kõik fotod
      photos.value = response;
    } catch (error) {
      console.error("Piltide laadimine ebaõnnestus:", error);
    }
  };

  // Lisab pildi teekonna andmebaasi
  const addPhoto = async (file: File, postId: number) => {
    const formData = new FormData();
    formData.append("file", file);
    formData.append("postId", postId.toString()); // Muudame postId stringiks

    try {
      const response = await api.customFetch<Photo>("/api/photos/upload", {
        method: "POST",
        body: formData,
      });
      if (response && response.id) {
        photos.value.push(response);
      }
    } catch (error) {
      console.error("Pildi salvestamine ebaõnnestus:", error);
    }
  };

  const removePhoto = async (photo: Photo) => {
    await api.customFetch(`api/photos/${photo.id}`, {
      method: "DELETE",
    });
    const index = photos.value.findIndex((p: Photo) => p.id === photo.id);
    if (index > -1) {
      photos.value.splice(index, 1);
    }
  };

  
const uploadPhoto = async (file: File, postId: number) => {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("postId", postId.toString());

  try {
    const response = await api.customFetch<Photo>("/api/photos/upload", {
      method: "POST",
      body: formData,
    });

    if (response) {
      photos.value.push(response); // Lisa uus foto lokaalsesse loendisse
      return response;
    }
  } catch (error) {
    console.error("Pildi üleslaadimine ebaõnnestus:", error);
    return null;
  }
};
  
  

  return { photos, addPhoto, removePhoto, loadPhotos, uploadPhoto};
});
