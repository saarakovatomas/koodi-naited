import { defineStore } from 'pinia';
import { ref } from 'vue';
import type { Checklist } from '~/types/checklist';
import { useApi } from '@/composables/useApi'; 
import { useAuth } from '~/composables/useAuth';

export const useChecklistStore = defineStore('checklist', () => {
  const api = useApi();
  const checklists = ref<Checklist[]>([]);

  // Load all checklists from the server
  const loadChecklist = async () => {
    const response = await api.customFetch<Checklist[]>("Checklists");
    checklists.value = response.map(checklist => ({
        ...checklist,
        items: checklist.items || [] 
    }));
  };

  const loadChecklistById = async (checklistId: number): Promise<Checklist | undefined> => {
    let checklist = checklists.value.find(c => c.id === checklistId);
    
    if (!checklist) {
      checklist = await api.customFetch<Checklist>(`Checklists/${checklistId}`);
      if (checklist) {
        checklists.value.push(checklist);  
      }
    }
    return checklist;
  };

  const addChecklist = async (checklist: Omit<Checklist, 'id'>) => {
    try {
      const response = await api.customFetch<Checklist>('Checklists', {
        method: 'POST',
        body: checklist,
      });

      if (response && response.id) {
        checklists.value.push(response);
      }
    } catch (error) {
      console.error("Failed to add checklist:", error);
    }
  };

  const updateChecklist = async (checklist: Checklist) => {
    const response = await api.customFetch<Checklist>(`Checklists/${checklist.id}`, {
      method: "PUT",
      body: checklist,
    });
  
    if (response && response.id) {
      const index = checklists.value.findIndex(c => c.id === checklist.id);
      if (index > -1) {
        checklists.value[index] = response;
      }
    }
  };

  const removeChecklist = async (checklist: Checklist) => {
    await api.customFetch(`Checklists/${checklist.id}`, {
      method: "DELETE",
    });

    const index = checklists.value.findIndex(c => c.id === checklist.id);
    if (index > -1) {
      checklists.value.splice(index, 1);
    }
  };

  return { checklists, loadChecklistById, loadChecklist, addChecklist, updateChecklist, removeChecklist };
});
