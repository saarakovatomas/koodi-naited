import { defineStore } from "pinia";
import { ref } from "vue";
import { useAuth } from "~/composables/useAuth";
import type { Budget } from "~/types/budget";

export const useBudgetStore = defineStore('budget', () => {
  const api = useApi();
  const budgets = ref<Budget[]>([]);

  // Laadi kõik eelarved, sealhulgas valuutainfo
  const loadBudget = async () => {
    const response = await api.customFetch<Budget[]>("Budgets");
    budgets.value = response;
  };

  // Laadi üks eelarve ID järgi, sealhulgas valuutainfo
  const loadBudgetById = async (budgetId: number): Promise<Budget | undefined> => {
    let budget = budgets.value.find(b => b.id === budgetId);
    
    if (!budget) {
      budget = await api.customFetch<Budget>(`Budgets/${budgetId}`);
  
      if (budget) {
        budgets.value.push(budget);  
      }
    }
    return budget;
  };

  // Eelarve lisamine
  const addBudget = async (budget: Omit<Budget, 'id'>) => {
    const response = await api.customFetch<Budget>("Budgets", {
      method: "POST",
      body: budget,
    });

    if (response && response.id) {
      budgets.value.push(response);  
    }
  };

  // Eelarve värskendamine (sh valuuta)
  const updateBudget = async (budget: Budget) => {
    const response = await api.customFetch<Budget>(`Budgets/${budget.id}`, {
      method: "PUT",
      body: budget,
    });

    if (response && response.id) {
  
      const index = budgets.value.findIndex(d => d.id === budget.id);
      if (index > -1) {
        budgets.value[index] = response;  
      }
    }
  };

  // Eelarve kustutamine
  const removeBudget = async (budget: Budget) => {
    await api.customFetch(`Budgets/${budget.id}`, {
      method: "DELETE",
    });

    const index = budgets.value.findIndex(d => d.id === budget.id);
    if (index > -1) {
      budgets.value.splice(index, 1); 
    }
  };

  return { budgets, addBudget, updateBudget, removeBudget, loadBudget, loadBudgetById };
});
