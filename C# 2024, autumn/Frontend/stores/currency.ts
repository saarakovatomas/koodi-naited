import { defineStore } from "pinia";
import { ref } from "vue";
import { useApi } from '~/composables/useApi';
import { useAuth } from "~/composables/useAuth";
import type { Currency } from '~/types/currency';  // Impordi tüüp

export const useCurrencyStore = defineStore('currency', () => {
  const api = useApi();
  const currencies = ref<Currency[]>([]);  // Täpsustame tüüpideks Currency[]

  // Laadi kõik valuutad
  const loadCurrencies = async () => {
    try {
      const response = await api.customFetch<Currency[]>("Currencies");  // Täpsustame, et tagastatakse Currency[] tüüp
      currencies.value = response;
    } catch (error) {
      console.error("Valuutade laadimine ebaõnnestus:", error);
    }
  };

  // Lisa uus valuuta
  const addCurrency = async (currency: Omit<Currency, 'id'>) => {
    try {
      const response = await api.customFetch<Currency>("Currencies", {
        method: "POST",
        body: currency,
      });
      if (response && response.id) {
        currencies.value.push(response);  // Lisa tagasi uus valuuta
      }
    } catch (error) {
      console.error("Valuuta lisamine ebaõnnestus:", error);
    }
  };

  // Uuenda olemasolev valuuta
  const updateCurrency = async (currency: Currency) => {
    try {
      const response = await api.customFetch<Currency>(`Currencies/${currency.id}`, {
        method: "PUT",
        body: currency,
      });
      if (response && response.id) {
        // Uuenda valuuta, mis on salvestatud poodi
        const index = currencies.value.findIndex(c => c.id === currency.id);
        if (index > -1) {
          currencies.value[index] = response;
        }
      }
    } catch (error) {
      console.error("Valuuta uuendamine ebaõnnestus:", error);
    }
  };

  // Kustuta valuuta
  const removeCurrency = async (currencyId: number) => {
    try {
      await api.customFetch(`Currencies/${currencyId}`, {
        method: "DELETE",
      });
      // Eemalda valuuta poodist
      currencies.value = currencies.value.filter(c => c.id !== currencyId);
    } catch (error) {
      console.error("Valuuta eemaldamine ebaõnnestus:", error);
    }
  };

  return { currencies, loadCurrencies, addCurrency, updateCurrency, removeCurrency };
});
