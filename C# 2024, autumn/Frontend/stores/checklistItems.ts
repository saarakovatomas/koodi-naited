import { defineStore } from 'pinia';
import { ref } from 'vue';
import type { ChecklistItem } from '~/types/checklistItem';
import { useApi } from '@/composables/useApi'; // Ensure this import is correct
import { useAuth } from '~/composables/useAuth';

export const useChecklistItemStore = defineStore('checklistItem', () => {
  const api = useApi();
  const checklistItems = ref<ChecklistItem[]>([]);

  // Load all checklist items from the server
  const loadChecklistItems = async () => {
    const response = await api.customFetch<ChecklistItem[]>("ChecklistItems");
    checklistItems.value = response;
  };

  const loadChecklistItemsByChecklistId = async (checklistId: number) => {
    const response = await api.customFetch<ChecklistItem[]>(`ChecklistItems?checklistId=${checklistId}`);
    checklistItems.value = response;
  };

  const loadChecklistItemById = async (itemId: number): Promise<ChecklistItem | undefined> => {
    let item = checklistItems.value.find(i => i.id === itemId);
    
    if (!item) {
      item = await api.customFetch<ChecklistItem>(`ChecklistItems/${itemId}`);
      if (item) {
        checklistItems.value.push(item);  // Cache the item in the store
      }
    }
    return item;
  };

  const addChecklistItem = async (item: Omit<ChecklistItem, 'id'>) => {
    try {
      const response = await api.customFetch<ChecklistItem>('ChecklistItems', {
        method: 'POST',
        body: item,
      });

      if (response && response.id) {
        checklistItems.value.push(response);
      }
    } catch (error) {
      console.error("Failed to add checklist item:", error);
    }
  };

  const updateChecklistItem = async (item: ChecklistItem) => {
    const response = await api.customFetch<ChecklistItem>(`ChecklistItems/${item.id}`, {
      method: "PUT",
      body: item,
    });
  
    if (response && response.id) {
      const index = checklistItems.value.findIndex(i => i.id === item.id);
      if (index > -1) {
        checklistItems.value[index] = response;
      }
    }
  };

  const removeChecklistItem = async (item: ChecklistItem) => {
    await api.customFetch(`ChecklistItems/${item.id}`, {
      method: "DELETE",
    });

    const index = checklistItems.value.findIndex(i => i.id === item.id);
    if (index > -1) {
      checklistItems.value.splice(index, 1);
    }
  };

  return { checklistItems, loadChecklistItems, loadChecklistItemsByChecklistId, loadChecklistItemById, addChecklistItem, updateChecklistItem, removeChecklistItem };
});