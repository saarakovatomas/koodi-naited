export type ChecklistItem = {
    id: number;             // Üksuse ID
    checklistId: number;    // Seotud nimekirja ID
    name: string;           // Üksuse nimi
    checked: boolean;       // Kas üksus on märgitud või mitte
    isCheckbox: boolean;    // Kas üksus on märkeruut (true) või alapealkiri (false)
  };