export type Currency = {
    id: number;         // Unikaalne identifikaator
    name: string;       // Valuuta nimi (nt EUR, USD)
};