export type Checklist = {
  id: number;              // Nimekirja ID
  title: string;           // Nimekirja pealkiri
  items: ChecklistItem[];  // Seotud üksuste massiiv
  checkedCount: number;    // Märgitud üksuste arv nimekirjas
  totalCount: number;      // Kokku märkeruutude arv nimekirjas
};