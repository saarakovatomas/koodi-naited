export type Diary = {
    id: number;
    title: string;
    description: string;
    imageSrc?: string; // Added Image property
    sharedWith: string[];
};