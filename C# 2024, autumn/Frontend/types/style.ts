export type Style = {
  diaryId: number;
  titleFontFamily: string;
  titleFontSize: string;
  titleColor: string;
  titleFontStyle: string;
  dateLocationFontFamily: string;
  dateLocationFontSize: string;
  dateLocationFontStyle: string;
  dateLocationTextAlign: string;
  dateLocationColor: string;
  showLocation: boolean;
  showDate: boolean;
  textFontFamily: string;
  textFontSize: string;
  textColor: string;
  selectedLineStyle: string;
  lineBorderTop: string;
  lineMarginTop: string;
  lineMarginBottom: string;
  showPageNumber: boolean;
  syncLineColor: boolean;
  id?: number
};
