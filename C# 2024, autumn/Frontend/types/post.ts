
export type Post = {
    id: number;             // Postituse unikaalne identifikaator
    diaryId: number,
    title?: string;          // Postituse pealkiri
    content?: string;        // Postituse sisu
    dateTime?: string;             // Postituse loomise või avaldamise kuupäev
    location?: string;      // Asukoht, kus postitus tehti
    photos: string[];
    pageNumber?: number;
    layout?: string;
}
