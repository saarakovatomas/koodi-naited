export type Countdown = {
    id: number;             // Loenduri unikaalne ID
    title: string;          // Loenduri pealkiri
    description?: string;   // (valikuline) loenduri kirjeldus
    targetDate: Date;       // Sihtkuupäev, mille suunas loendur töötab
    isActive: boolean;      // Näitab, kas loendur on aktiivne
};