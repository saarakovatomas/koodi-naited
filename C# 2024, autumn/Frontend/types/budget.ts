export type Budget ={
    id: number;
    category:string;
    amount:number;
    currency:string;
    expense:number;
};