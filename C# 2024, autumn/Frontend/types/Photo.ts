export type Photo = {
    id: number; 
    imageSrc: string;
}