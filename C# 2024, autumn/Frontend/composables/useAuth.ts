import type { User } from "~/types/user";
import type { NitroFetchOptions, NitroFetchRequest } from "nitropack";

export const useAuth = () => {
  const activeToken = useState<string | undefined>("token", () => {
    if (process.client) {
      // Kontrolli, kas oleme brauseris
      return localStorage.getItem("token") || undefined;
    }
    return undefined; // SSR-i puhul
  });

  const api = useApi();

  const isAuthenticated = computed(() => !!activeToken.value);

  const logIn = async (user: User) => {
    try {
      const token = await api.customFetch<{ token: string }>("users/login", {
        method: "POST",
        body: user,
      });
  
      activeToken.value = token.token;
  
      if (process.client) {
        // Save the token in localStorage
        localStorage.setItem("token", token.token);
      }
  
      return { success: true };
    } catch (e: unknown) {
      console.error("Login failed", e);
  
      // Return an error message for the UI
      return { success: false, message: "Vale kasutajanimi või parool" };
    }
  };
  

  const logOut = async () => {
    activeToken.value = undefined;
    if (process.client) {
      // Eemalda token `localStorage`-ist ainult kliendipoolselt
      localStorage.removeItem("token");
    }
    location.replace("/");
  };
  const register = async (user: User) => {
    try {
      const response = await api.customFetch<{ message?: string }>("users/register", {
        method: "POST",
        body: user,
      });
      return { success: true, message: response?.message || "Registreerimine õnnestus" };
    } catch (e: unknown) {
      if (e instanceof Error) {
        console.error("Registreerimine ebaõnnestus");
        return { success: false, message: "Registreerimine ebaõnnetus: kasutaja on juba olemas "};
      }
      console.error("An unknown error occurred during registration", e);
      return { success: false, message: "An unknown error occurred during registration" };
    }
  };
  
  

  const fetchWithToken = async <T>(
    url: string,
    options?: NitroFetchOptions<NitroFetchRequest>
  ) => {
    return await api.customFetch<T>(url, {
      ...options,
      headers: {
        Authorization: "Bearer " + activeToken.value,
        ...options?.headers,
      },
    });
  };

  const changePassword = async (username: string, oldPassword: string, newPassword: string) => {
    try {
      const response = await api.customFetch<{ message: string }>("users/change-password", {
        method: "POST",
        body: {
          username,
          password: oldPassword,
          newPassword,
        },
        headers: {
          Authorization: "Bearer " + activeToken.value,
        },
      });
      console.log(response.message);
      return true;
    } catch (e) {
      console.error("Password change failed", e);
      return false;
    }
  };

  const getUserByUsername = async (username: string) => {
    try {
      const response = await api.customFetch<User>("user/" + username, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + activeToken.value,
        },
      });

      return response;
    } catch (e) {
      console.error("Failed to fetch user", e);
      return null;
    }
  };

  return {
    logIn,
    logOut,
    register,
    isAuthenticated,
    fetchWithToken,
    changePassword,
    getUserByUsername,
  };
};
