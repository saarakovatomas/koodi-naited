using System.Threading.Tasks;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos
{
    public class StylesRepo(DataContext context)
    {
        private readonly DataContext context = context;

        // Uue stiili salvestamine andmebaasi
        public async Task<Style> SaveStyleToDb(Style style){
            context.Add(style);
            await context.SaveChangesAsync();
            return style;
        }

        // Kõikide stiilide lugemine andmebaasist
        public async Task<List<Style>> GetAllStyles() {
            IQueryable<Style> query = context.StyleList.AsQueryable();
            return await query.ToListAsync();
        }

        // // stiilide saamine id järgi andmebaasist
        public async Task<Style?> GetStyleById(int id) => await context.StyleList.FindAsync(id);


        // Boolean, kas stiil eksisteerib andmebaasis
        public async Task<bool> StyleExistsInDb(int id) => await context.StyleList.AnyAsync(x => x.Id == id);

        // stiili uuendamine

        public async Task<bool> UpdateStyle(int id, Style style) {
            bool isIdsMatch = id == style.Id;
            bool diaryExists = await StyleExistsInDb(id);

            if (!isIdsMatch || !diaryExists) {
                return false;
            }

            context.Update(style);
            int updatedRecordsCount = await context.SaveChangesAsync();
            return updatedRecordsCount == 1;
        }

         // stiili kustutamine andmebaasist
        public async Task<bool> DeleteStyleById(int id) {
            Style? styleInDb = await GetStyleById(id);
            if (styleInDb is null) {
                return false;
            }
            context.Remove(styleInDb);
            int changesCount = await context.SaveChangesAsync();
            return changesCount == 1;
        }  
    }
}
