using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Models.Classes;
using ITB2203_2024_traveldiary.Data;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Data.Repos
{
    public class BudgetsRepo(DataContext context)
    {
        private readonly DataContext context=context;

        public async Task<Budget> SaveBudgetToDb(Budget budget)
        {
             context.BudgetList.Add(budget);
             await context.SaveChangesAsync();
             return budget;
        }
        public async Task<List<Budget>> GetAllBudgets() {
            IQueryable<Budget> query = context.BudgetList.AsQueryable();
            return await query.ToListAsync();
        }

        public async Task<Budget?> GetBudgetById(int id) => await context.BudgetList.FindAsync(id);

        public async Task<bool> BudgetExistsInDb(int id) => await context.BudgetList.AnyAsync(x => x.Id == id);
         public async Task<bool> UpdateBudget(int id, Budget budget) {
            bool isIdsMatch = id == budget.Id;
            bool diaryExists = await BudgetExistsInDb(id);

            if (!isIdsMatch || !diaryExists) {
                return false;
            }
            context.Update(budget);
            int updatedRecordsCount = await context.SaveChangesAsync();
            return updatedRecordsCount == 1;
        }

         public async Task<bool> DeleteBudgetById(int id) {
            Budget? budgetInDb = await GetBudgetById(id);
            if (budgetInDb is null) {
                return false;
            }
            context.Remove(budgetInDb);
            int changesCount = await context.SaveChangesAsync();
            return changesCount == 1;
        }
    }
}