using ITB2203_2024_traveldiary.Models.Classes;
using ITB2203_2024_traveldiary.Data;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos;

    public class PhotosRepo(DataContext context ){
        private readonly DataContext context = context;

        public async Task<Photo> SavePhotoToDb(Photo photo){
            context.Add(photo);
            await context.SaveChangesAsync();
            return photo;
        }
        public async Task<List<Photo>> GetAllPhotos() {
            IQueryable<Photo> query = context.PhotoList.AsQueryable();
            return await query.ToListAsync();
        }
        public async Task<Photo?> GetPhotoById(int id) => await context.PhotoList.FindAsync(id);
        public async Task<bool> PhotoExistsInDb(int id) => await context.PhotoList.AnyAsync(x => x.Id == id);
        public async Task<bool> DeletePhotoById(int id) {
            Photo? photoInDb = await GetPhotoById(id);
            if (photoInDb is null) {
                return false;
            }
            context.Remove(photoInDb);
            int changesCount = await context.SaveChangesAsync();
            return changesCount == 1;
        }

    public static implicit operator PhotosRepo(PostsRepo v)
    {
        throw new NotImplementedException();
    }
}
        
    
