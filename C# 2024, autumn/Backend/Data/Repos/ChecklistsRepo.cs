using ITB2203_2024_traveldiary.Models.Classes;
using ITB2203_2024_traveldiary.Data;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos
{
    public class ChecklistsRepo(DataContext context)
    {
        private readonly DataContext context = context;

        // Salvestab uue nimekirja andmebaasi
        public async Task<Checklist> SaveChecklistToDb(Checklist checklist)
        {
            context.Add(checklist); // Lisa uus nimekiri andmebaasi
            await context.SaveChangesAsync(); // Salvesta muudatused
            return checklist; // Tagasta salvestatud nimekiri
        }

        // Toob kõik nimekirjad andmebaasist
        public async Task<List<Checklist>> GetAllChecklists()
        {
            return await context.ChecklistList
                .Include(c => c.Items) // Kaasa seotud punktid nimekirjadega
                .ToListAsync(); // Tagasta kõik nimekirjad
        }

        // Toob nimekirja ID alusel
        public async Task<Checklist?> GetChecklistById(int id) => await context.ChecklistList.FindAsync(id);

        // Kontrollib, kas nimekiri eksisteerib andmebaasis
        public async Task<bool> ChecklistExistsInDb(int id) => await context.ChecklistList.AnyAsync(x => x.Id == id);

        // Uuendab olemasolevat nimekirja
        public async Task<bool> UpdateChecklist(int id, Checklist checklist)
        {
            bool isIdsMatch = id == checklist.Id; // Kontrollib, kas URL-i ID ja keha ID on samad
            bool checklistExists = await ChecklistExistsInDb(id); // Kontrollib, kas nimekiri eksisteerib andmebaasis

            if (!isIdsMatch || !checklistExists)
            {
                return false; // Tagasta false, kui ID-d ei ühti või nimekirja ei eksisteeri
            }

            context.Update(checklist); // Uuenda nimekirja andmebaasis
            int updatedRecordsCount = await context.SaveChangesAsync(); // Salvesta muudatused
            return updatedRecordsCount == 1; // Tagasta true, kui muudatused õnnestusid
        }

        // Kustutab nimekirja ID alusel
        public async Task<bool> DeleteChecklistById(int id)
        {
            Checklist? checklistInDb = await GetChecklistById(id); // Leia nimekiri ID alusel
            if (checklistInDb is null)
            {
                return false; // Tagasta false, kui nimekirja ei leitud
            }

            context.Remove(checklistInDb); // Kustuta nimekiri andmebaasist
            int changesCount = await context.SaveChangesAsync(); // Salvesta muudatused
            return changesCount == 1; // Tagasta true, kui kustutamine õnnestus
        }
    }
}