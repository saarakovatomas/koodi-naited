using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BackEnd.Models.Classes;
using ITB2203_2024_traveldiary.Data;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;



namespace ITB2203_2024_traveldiary.Data.Repos;

    public class UsersRepo
    {
       private readonly DataContext _context;
        private IConfiguration _config;


        public UsersRepo(IConfiguration config, DataContext context)
        {
            _config = config;
            _context = context;
        }

        public async Task<string> Login([FromBody] User login)
        {
            var dbUser = await _context.UserList!.FirstOrDefaultAsync(user => user.Username == login.Username);

            if (dbUser == null || dbUser.Password != HashPassword(login.Password)){
                return "";
            }

            login.OrganizationId = dbUser.OrganizationId;
            return GenerateJSONWebToken(login);
        }

        private string HashPassword(string password)
        {
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: [],
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            return hashed;
        }

        private string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                new List<Claim> { new Claim("organizationId", user.OrganizationId.ToString()) },
                expires: DateTime.Now.AddMinutes(60),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public async Task<bool> ChangePassword(string username, string oldPassword, string newPassword)
        {

            var dbUser = await _context.UserList!.FirstOrDefaultAsync(user => user.Username == username);

            if (dbUser == null || dbUser.Password != HashPassword(oldPassword))
            {
  
             return false;
            }
            dbUser.Password = HashPassword(newPassword);
            _context.Update(dbUser);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Register(User newUser)
        {
            if (await _context.UserList!.AnyAsync(user => user.Username == newUser.Username))
            {
                // Username already exists
                return false;
            }
    
            newUser.Password = HashPassword(newUser.Password);
            await _context.UserList!.AddAsync(newUser);
            await _context.SaveChangesAsync();
            return true;
        }

          public async Task<User?> GetUserByUsername(string username)
        {
            var dbUser = await _context.UserList!.FirstOrDefaultAsync(user => user.Username == username);

            return dbUser; // Will return null if no user is found
        }




}
