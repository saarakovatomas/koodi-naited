using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITB2203_2024_traveldiary.Models.Classes;
using ITB2203_2024_traveldiary.Data;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos
{
    public class PostsRepo(DataContext context)
    {
        private readonly DataContext context = context;

        // Uue postituse salvestamine andmebaasi
        public async Task<Post> SavePostToDb(Post post){
            context.Add(post);
            await context.SaveChangesAsync();
            return post;
        }

        // Kõikide postituste lugemine andmebaasist
        public async Task<List<Post>> GetAllPosts() {
            IQueryable<Post> query = context.PostList.AsQueryable();
            return await query.ToListAsync();
        }

        // // Postituse saamine id järgi andmebaasist
        public async Task<Post?> GetPostById(int id) => await context.PostList.FindAsync(id);


        // Boolean, kas postitus eksisteerib andmebaasis
        public async Task<bool> PostExistsInDb(int id) => await context.PostList.AnyAsync(x => x.Id == id);

        // Postituse uuendamine

        public async Task<bool> UpdatePost(int id, Post post) {
            bool isIdsMatch = id == post.Id;
            bool diaryExists = await PostExistsInDb(id);

            if (!isIdsMatch || !diaryExists) {
                return false;
            }

            context.Update(post);
            int updatedRecordsCount = await context.SaveChangesAsync();
            return updatedRecordsCount == 1;
        }

         // Postituse kustutamine andmebaasist
        public async Task<bool> DeletePostById(int id) {
            Post? postInDb = await GetPostById(id);
            if (postInDb is null) {
                return false;
            }
            context.Remove(postInDb);
            int changesCount = await context.SaveChangesAsync();
            return changesCount == 1;
        }  
    }
}