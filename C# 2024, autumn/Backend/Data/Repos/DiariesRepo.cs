using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos
{
    public class DiariesRepo(DataContext context)
    {
        private readonly DataContext context = context;
        
        // Uue päeviku salvestamine andmebaasi
        public async Task<Diary> SaveDiaryToDb(Diary diary)
        {
            context.Add(diary);
            await context.SaveChangesAsync();
            return diary;
        }

        // Kõikide päevikute lugemine andmebaasist
        public async Task<List<Diary>> GetAllDiaries() {
            IQueryable<Diary> query = context.DiaryList.AsQueryable();
            return await query.ToListAsync();
        }
        
        // Päeviku saamine id järgi andmebaasist
        public async Task<Diary?> GetDiaryById(int id) => await context.DiaryList.FindAsync(id);

        // Boolean, kas päevik eksisteerib andmebaasis
        public async Task<bool> DiaryExistsInDb(int id) => await context.DiaryList.AnyAsync(x => x.Id == id);

        // Päeviku uuendamine
        public async Task<bool> UpdateDiary(int id, Diary diary) {
            bool isIdsMatch = id == diary.Id;
            bool diaryExists = await DiaryExistsInDb(id);

            if (!isIdsMatch || !diaryExists) {
                return false;
            }

            context.Update(diary);
            int updatedRecordsCount = await context.SaveChangesAsync();
            return updatedRecordsCount == 1;
        }

        // Päeviku kustutamine andmebaasist
        public async Task<bool> DeleteDiaryById(int id) {
            Diary? diaryInDb = await GetDiaryById(id);
            if (diaryInDb is null) {
                return false;
            }
            context.Remove(diaryInDb);
            int changesCount = await context.SaveChangesAsync();
            return changesCount == 1;
        }
    }
}