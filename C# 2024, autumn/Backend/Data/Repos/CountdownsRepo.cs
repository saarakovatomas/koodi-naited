using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos
{
    public class CountdownsRepo
    {
        private readonly DataContext _context;

        public CountdownsRepo(DataContext context)
        {
            _context = context;
        }

        // Uue loenduri salvestamine andmebaasi
        public async Task<Countdown> SaveCountdownToDb(Countdown countdown)
        {
            if (countdown.TargetDate <= DateTime.UtcNow)
        {
            throw new ArgumentException("TargetDate must be in the future.");
        }
            _context.Add(countdown);
            await _context.SaveChangesAsync();
            return countdown;
        }

        // Kõikide loendurite lugemine andmebaasist
        public async Task<List<Countdown>> GetAllCountdowns()
        {
            return await _context.CountdownList.ToListAsync();
        }

        // Loenduri saamine ID järgi
        public async Task<Countdown?> GetCountdownById(int id)
        {
            return await _context.CountdownList.FindAsync(id);
        }

        // Boolean, kas loendur eksisteerib andmebaasis
        public async Task<bool> CountdownExistsInDb(int id)
        {
            return await _context.CountdownList.AnyAsync(x => x.Id == id);
        }

        // Loenduri uuendamine
        public async Task<bool> UpdateCountdown(int id, Countdown countdown)
        {
            bool isIdsMatch = id == countdown.Id;
            bool countdownExists = await CountdownExistsInDb(id);

            if (!isIdsMatch || !countdownExists)
            {
                return false;
            }

            _context.Update(countdown);
            int updatedRecordsCount = await _context.SaveChangesAsync();
            return updatedRecordsCount == 1;
        }

        // Loenduri kustutamine andmebaasist
        public async Task<bool> DeleteCountdownById(int id)
        {
            Countdown? countdownInDb = await GetCountdownById(id);
            if (countdownInDb is null)
            {
                return false;
            }
            _context.Remove(countdownInDb);
            int changesCount = await _context.SaveChangesAsync();
            return changesCount == 1;
        }
    }
}
