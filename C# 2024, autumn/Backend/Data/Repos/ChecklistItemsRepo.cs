using ITB2203_2024_traveldiary.Models.Classes;
using ITB2203_2024_traveldiary.Data;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data.Repos
{
    public class ChecklistItemsRepo(DataContext context)
    {
        private readonly DataContext context = context;

        // Salvestab uue nimekirja punkti andmebaasi
        public async Task<ChecklistItem> SaveChecklistItemToDb(ChecklistItem checklistItem)
        {
            context.Add(checklistItem); // Lisa uus punkt andmebaasi
            await context.SaveChangesAsync(); // Salvesta muudatused
            return checklistItem; // Tagasta salvestatud punkt
        }

        // Toob kõik nimekirja punktid andmebaasist
        public async Task<List<ChecklistItem>> GetAllChecklistItems()
        {
            IQueryable<ChecklistItem> query = context.ChecklistItemList.AsQueryable(); // Koosta päring kõigi punktide saamiseks
            return await query.ToListAsync(); // Tagasta punktide nimekiri
        }

        // Toob nimekirja punkti ID alusel
        public async Task<ChecklistItem?> GetChecklistItemById(int id) => await context.ChecklistItemList.FindAsync(id);

        // Toob kõik nimekirja punktid vastavalt nimekirja ID-le
        public async Task<List<ChecklistItem>> GetChecklistItemsByChecklistId(int checklistId)
        {
            return await context.ChecklistItemList
                .Where(item => item.ChecklistId == checklistId) // Filtreeri punktid vastavalt nimekirja ID-le
                .ToListAsync(); // Tagasta leitud punktid
        }

        // Kontrollib, kas nimekirja punkt eksisteerib andmebaasis
        public async Task<bool> ChecklistItemExistsInDb(int id) => await context.ChecklistItemList.AnyAsync(x => x.Id == id);

        // Uuendab olemasolevat nimekirja punkti
        public async Task<bool> UpdateChecklistItem(int id, ChecklistItem checklistItem)
        {
            bool isIdsMatch = id == checklistItem.Id; // Kontrollib, kas URL-i ID ja keha ID on samad
            bool itemExists = await ChecklistItemExistsInDb(id); // Kontrollib, kas punkt eksisteerib andmebaasis

            if (!isIdsMatch || !itemExists)
            {
                return false; // Tagasta false, kui ID-d ei ühti või punkti ei eksisteeri
            }

            context.Update(checklistItem); // Uuenda punkti andmebaasis
            int updatedRecordsCount = await context.SaveChangesAsync(); // Salvesta muudatused
            return updatedRecordsCount == 1; // Tagasta true, kui muudatused õnnestusid
        }

        // Kustutab nimekirja punkti ID alusel
        public async Task<bool> DeleteChecklistItemById(int id)
        {
            ChecklistItem? itemInDb = await GetChecklistItemById(id); // Leia punkt ID alusel
            if (itemInDb is null)
            {
                return false; // Tagasta false, kui punkti ei leitud
            }

            context.Remove(itemInDb); // Kustuta punkt andmebaasist
            int changesCount = await context.SaveChangesAsync(); // Salvesta muudatused
            return changesCount == 1; // Tagasta true, kui kustutamine õnnestus
        }
    }
}