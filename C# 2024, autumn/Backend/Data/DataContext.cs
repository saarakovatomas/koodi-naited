using BackEnd.Models.Classes;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.EntityFrameworkCore;

namespace ITB2203_2024_traveldiary.Data
{
    public class DataContext(DbContextOptions<DataContext> options) : DbContext(options)
    {
        public DbSet<Diary> DiaryList { get; set; }
        public DbSet<Photo> PhotoList{ get; set;}
        public DbSet<Post> PostList{ get; set;}
        public DbSet <Budget> BudgetList {get;set;}
        public DbSet<Countdown> CountdownList { get; set;}
        public DbSet<Checklist> ChecklistList { get; set;}
        public DbSet<ChecklistItem> ChecklistItemList { get; set;}
        public DbSet<Style> StyleList { get; set;}
        public DbSet<User>? UserList { get; set;}
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Diary>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Diary>().Property(p => p.Id).HasIdentityOptions(startValue: 1); 

            modelBuilder.Entity<Photo>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Photo>().Property(p => p.Id).HasIdentityOptions(startValue: 1); 

            modelBuilder.Entity<Post>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Post>().Property(p => p.Id).HasIdentityOptions(startValue: 1);

            modelBuilder.Entity<Budget>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Budget>().Property(p => p.Id).HasIdentityOptions(startValue: 1);

            modelBuilder.Entity<Countdown>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Countdown>().Property(p => p.Id).HasIdentityOptions(startValue: 1);

            modelBuilder.Entity<Checklist>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Checklist>()
                    .HasMany(c => c.Items) // Nimekirjal võib olla mitu itemit
                    .WithOne() // Iga item kuulub ühele nimekirjale
                    .HasForeignKey(ci => ci.ChecklistId);

            modelBuilder.Entity<ChecklistItem>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ChecklistItem>().Property(x => x.ChecklistId).IsRequired();
            
            modelBuilder.Entity<Style>().Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Style>().Property(p => p.Id).HasIdentityOptions(startValue: 1);





           /*
            // Näiteloenduri kirje lisamine
            modelBuilder.Entity<Countdown>().HasData(
                new Countdown
                {
                    Id = 1,
                    Title = "Aastavahetus",
                    Description = "Aastavahetuseni on jäänud täpselt nii kaua aega",
                    TargetDate = DateTime.SpecifyKind(new DateTime(2024, 12, 31, 23, 59, 59), DateTimeKind.Utc),
                    IsActive = true
                }
            ); */

            modelBuilder.Entity<Diary>().HasData(
                new Diary {
                    Id = 1,
                    Title = "Rooma",
                    Description = "Kevad 2023",
                    SharedWith = [], 
                    ImageSrc = "/backgrounds/city.png",   
                },
                new Diary {
                    Id = 2,
                    Title = "Bali",
                    Description = "Sügis 2023",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/sea.png",   
                },
                new Diary {
                    Id = 3,
                    Title = "Šveits",
                    Description = "jaanuar 2021",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/mountains.png",   
                },

                new Diary {
                    Id = 4,
                    Title = "Seiklus",
                    Description = "2020",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/plane_and_ship.png",   
                },

                new Diary {
                    Id = 5,
                    Title = "Cappadocia",
                    Description = "Juuni 2019",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/air.png",   
                },
                

                new Diary {
                    Id = 6,
                    Title = "Sri Lanka",
                    Description = "2022",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/beach.png",   
                },

                new Diary {
                    Id = 7,
                    Title = "Celebration",
                    Description = "2021",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/celebration.png",   
                },

                new Diary {
                    Id = 8,
                    Title = "Austraalia",
                    Description = "2017",
                    SharedWith = [],
                    ImageSrc = "/backgrounds/map.png",   
                }
            );

            modelBuilder.Entity<Photo>().HasData(
                new Photo {
                    Id = 1,
                    ImageSrc = "/images/photo1.jpg"
                    
                },
                new Photo {
                    Id = 2,
                    ImageSrc = "/images/photo2.jpg"
                },
                new Photo {
                    Id = 3,
                    ImageSrc = "/images/photo3.jpg"
                }
            );
            
            modelBuilder.Entity<Post>().HasData(
                new Post {
                    Id = 1,
                    DiaryId = 2,
                    Title =  " Uluwatu ja surf",
                    Content = "Tänase päeva veetsin Uluwatu legendaarsetel lainetel. Hommik algas varakult, kui suundusin rannale, et vältida suurimat rahvamassi ja saada parimad lained. Surfamine Uluwatu pikal ja kiirel lainel oli adrenaliinirohke, lained olid jõulised ja väljakutseid pakkuvad. Pärast mitut tundi vees, puhkasin rannas ja nautisin kohaliku warungi värskeid puuvilju ja külma Bintangi.Õhtupoolikul jalutasin Uluwatu templi juurde, kust avanes võrratu vaade ookeanile päikeseloojangul. Päeva lõpetasin lõkke ääres, kus kohtusin teiste surfaritega ja jagasime omavahel päeva muljeid. Täiuslik päev!",
                    DateTime = "11.03.2022",
                    Location = "Batu Bolong",
                    Photos = [],
                    PageNumber = 1,
                    Layout = "",
                    
                },
                new Post {
                    Id = 2,
                    DiaryId = 2,
                    Title =  " Gili saared",
                    Content = "Saabusin Gili saartele, mis on tuntud oma kristallselge vee ja rahulike randade poolest. Hommikul rentisin jalgratta ja sõitsin ümber Gili Trawangani, mis on suurim saartest, nautides vaikust ja auto-vaba keskkonda. Peatudes mitmel rannal, snorkeldasin värvikirevate korallide ja kirjude kalade seas. Lõunaks maitsesin värsket mereanni BBQ-d, mille kõrvale jõin külma kookosvee. Pärastlõunal puhkasin võrkkiiges, vaadates üle türkiissinise vee naabersaarte siluette.",
                    DateTime = "10.03.2022",
                    Location = "Gili Trawangan",
                    Photos = [],
                    PageNumber = 2,
                    Layout = "",
                },
                new Post {
                    Id = 3,
                    DiaryId = 3,
                    Title =  " Pealkiri 3",
                    Content = "vahva reis",
                    DateTime = "21.05.2012",
                    Location = "Eesti",
                    Photos = [],
                    PageNumber = 1,
                    Layout = "",
                },
                 new Post {
                    Id = 4,
                    DiaryId = 1,
                    Title =  " Pealkiri 4",
                    Content = "tore reis",
                    DateTime = "12.03.2022",
                    Location = "Itaalia",
                    Photos = [],
                    PageNumber = 1,
                    Layout = "",
                },
                 new Post {
                    Id = 5,
                    DiaryId = 2,
                    Title =  "Canggu",
                    Content = "Alustasin hommikut Echo Beachil, kus suundusin koos teiste surfifriikidega lainetele. Surfamine siin on alati äge, lained on täpselt paraja suurusega ja vesi mõnusalt soe. Pärast paari tundi aktiivset sõitu ja mõnda õnnestunud (ja mitte nii õnnestunud) lainet, olin valmis hommikusöögiks. Istusin maha ühes rannalähedases kohvikus, kus sõin imemaitsvat draakonivilja smuutikausi, mis oli kaetud värskete troopiliste puuviljadega — täiuslik Bali hommikusöök! ",
                    DateTime = "12.03.2022",
                    Location = "Echo Beach",
                    Photos = [],
                    PageNumber = 3,
                    Layout = "",
                },
                 new Post {
                    Id = 6,
                    DiaryId = 2,
                    Title =  "Ubudi seiklus",
                    Content = "Täna veetsin imelise päeva Ubudi südames, mis on tuntud oma kunsti ja kultuuri poolest. Alustasin päeva matkaga Campuhan Ridge Walkil, kus rohelised maastikud ja jõeorud pakkusid hingematvaid vaateid. Lõunaks proovisin kohalikus warungis traditsioonilist nasi gorengit, mis oli aromaatne ja maitseküllane. Pärastlõunal külastasin kuulsat kuninglikku paleed ning jalutasin Ubudi turgudel",
                    DateTime = "13.03.2022",
                    Location = "Ubud Old Market",
                    Photos = [],
                    PageNumber = 4,
                    Layout = "",
                },
                 new Post {
                    Id = 7,
                    DiaryId = 2,
                    Title =  "Nusa Penida",
                    Content = "Täna otsustasime minna Kelingking Beachile, kuid teistmoodi – otse paadiga, vältimaks järsku ja keerulist matka mööda kaljunõlva. Hommikul asusime teele väikese paadiga, kuid meri oli ootamatult tormine. Lained olid hiiglaslikud, mis muutis sõidu üsna hirmutavaks. Lähenedes rannale, saime aru, et paadiga otse liivaribale sõitmine on võimatu. Otsustasime ankrusse jääda veidi kaugemale ja ujuda ülejäänud osa. Merevool oli tugev ja lained pidevalt üle pea käivad, mis tegi ujumise keeruliseks ja adrenaliinirohkeks. Korduvalt tundsin, kuidas vesi tahtis mind tagasi merre tõmmata. Ujusime kõik koos, püüdes üksteist toetada ja julgustada.",
                    DateTime = "23.03.2022",
                    Location = "Nusa Penida",
                    Photos = [],
                    PageNumber = 5,
                    Layout = "",
                 });
            modelBuilder.Entity<Checklist>().HasData(
                new Checklist
                {
                    Id = 1,
                    Title = "Kohvrisse",
                    CheckedCount = 1,
                    TotalCount = 3
                },
                new Checklist
                {
                    Id = 2,
                    Title = "Itaalia linnad",
                    CheckedCount = 0,
                    TotalCount = 2
                });
            modelBuilder.Entity<ChecklistItem>().HasData(
                new ChecklistItem
                {
                    Id = 1,
                    ChecklistId = 1,
                    Name = "Pass",
                    Checked = true,
                    IsCheckbox = true
                },
                new ChecklistItem
                {
                    Id = 2,
                    ChecklistId = 1,
                    Name = "Päikesekreem",
                    Checked = false,
                    IsCheckbox = true
                },
                new ChecklistItem
                {
                    Id = 3,
                    ChecklistId = 1,
                    Name = "Rätik",
                    Checked = false,
                    IsCheckbox = true
                },
                new ChecklistItem
                {
                    Id = 4,
                    ChecklistId = 2,
                    Name = "Palermo",
                    Checked = false,
                    IsCheckbox = true
                },
                new ChecklistItem
                {
                    Id = 5,
                    ChecklistId = 2,
                    Name = "Milano",
                    Checked = false,
                    IsCheckbox = true
                });
            modelBuilder.Entity<Style>().HasData(
                new Style {
                    Id = 1,
                    DiaryId = 1,
                    TitleFontFamily = "monospace", 
                    TitleFontSize = "24pt", 
                    TitleFontStyle = "bold",
                    TitleColor = "#9D8C8B",
                    DateLocationFontFamily = "monospace", 
                    DateLocationFontSize = "9pt", 
                    DateLocationFontStyle = "italic",
                    DateLocationColor = "#9D8C8B",
                    DateLocationTextAlign = "Left",
                    ShowDate = true,
                    ShowLocation = true,
                    TextFontFamily = "monospace",
                    TextFontSize = "9pt",
                    TextColor = "#9D8C8B",
                    SelectedLineStyle = "Double lines",
                    LineBorderTop = "1px",
                    LineMarginTop = "10px",
                    LineMarginBottom = "10px",
                    ShowPageNumber = true
                });
                  modelBuilder.Entity<User>().HasData(
                    new User
                    {
                        Id = 1,
                        Username = "test1",
                        // parool on test1
                        Password = "St9tpNN2zrinRGNUgKWCy4JjZRFEorSQ0Zg3a/8m7k4=",
                        OrganizationId = 1, 
                    },
                    new User
                    {
                        Id = 2,
                        Username = "test2",
                        Password = "zWoe4T9h2Hj9G4dyUtWwcKwV6zMR1Q0yr3Uch+xSze8=", // test2
                        OrganizationId = 1, 
                    },
                    new User
                    {
                        Id = 3,
                        Username = "test3",
                        Password = "6RwNz8ehCp0yZ0KkUE7i+Shy+2l7C1Eh9dT/RULwZN8=", // test3
                        OrganizationId = 2, 
                    }
                );
        }
    }
 }
    