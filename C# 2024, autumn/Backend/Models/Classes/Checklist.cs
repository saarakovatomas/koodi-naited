using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITB2203_2024_traveldiary.Models.Classes
{
    public class Checklist
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public int CheckedCount { get; set; }
        public int TotalCount { get; set; }
        public List<ChecklistItem>? Items { get; set; }
    }
}