using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITB2203_2024_traveldiary.Models.Classes
{
    public record Countdown
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public DateTime TargetDate { get; set; } = DateTime.SpecifyKind(new DateTime(2024, 12, 31, 23, 59, 59), DateTimeKind.Utc);
    public bool IsActive { get; set; }
    public string? Description { get; set; }
}
}