using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITB2203_2024_traveldiary.Models.Classes
{
    public class ChecklistItem
    {
        public int Id { get; set;}
        public int ChecklistId { get; set; }
        public string? Name { get; set; }
        public bool Checked { get; set; }
        public bool IsCheckbox { get; set; }
    }
}