using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITB2203_2024_traveldiary.Models.Classes
{
    public record Post
    {
        public int Id { get; set; }
        public int DiaryId  { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public string? DateTime { get; set; }
        public string? Location { get; set; }
        public List<string>? Photos { get; set; }
        public int PageNumber { get; set; }
        public int? StyleId { get; set; }
        public string? Layout { get; set; }
    }
}