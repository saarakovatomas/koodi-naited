namespace BackEnd.Models.Classes;

    public class User{
        public int Id { get; set; }
        public string Username { get; set; } = "";
        public string Password { get; set; } = ""; 
        public string NewPassword { get; set; }="";
        public int OrganizationId { get; set; }   

    }
