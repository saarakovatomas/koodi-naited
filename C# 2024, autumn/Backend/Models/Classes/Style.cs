namespace ITB2203_2024_traveldiary.Models.Classes
{
    public class Style
    {
        public int Id { get; set; } // By convention, EF will treat this as the primary key
        public int DiaryId { get; set; } // Foreign key to associate with a diary

        // Style attributes
        public string? TitleFontFamily { get; set; }
        public string? TitleFontSize { get; set; }
        public string? TitleColor { get; set; }
        public string? TitleFontStyle { get; set; }
        public string? DateLocationFontFamily { get; set; }
        public string? DateLocationFontSize { get; set; }
        public string? DateLocationFontStyle { get; set; }
        public string? DateLocationTextAlign { get; set; }
        public string? DateLocationColor { get; set; }
        public bool ShowLocation { get; set; }
        public bool ShowDate { get; set; }
        public string? TextFontFamily { get; set; }
        public string? TextFontSize { get; set; }
        public string? TextColor { get; set; }
        public string? SelectedLineStyle { get; set; }
        public string? LineBorderTop { get; set; }
        public string? LineMarginTop { get; set; }
        public string? LineMarginBottom { get; set; }
        public bool ShowPageNumber { get; set; }
        public bool SyncLineColor { get; set; }
    }
}
