namespace ITB2203_2024_traveldiary.Models.Classes
{
    public record Diary
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public List<string>? SharedWith { get; set; }
        public string? ImageSrc { get; set; }
    }   
}