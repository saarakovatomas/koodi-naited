using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Models.Classes
{
    public record Budget
    {
        public int Id {get;set;}
        public decimal Expenses { get; set;}
        public decimal MoneyAmount { get; set;}

    }
}