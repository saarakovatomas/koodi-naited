namespace ITB2203_2024_traveldiary.Models.Classes;

    public record Photo{
        public int Id {get;set;}
        public string? ImageSrc {get; set;} 
    }
