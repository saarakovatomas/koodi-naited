using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ITB2203_2024_traveldiary.Data;
using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ITB2203_2024_traveldiary.Controllers
{
    [ApiController]
    [Route("api/diaries/{diaryId:int}/styles")]
    public class DiariesStylesController(DataContext context) : ControllerBase()
    {
        private readonly DataContext context = context;

        [HttpGet]
        public IActionResult GetDiaryStyle(int diaryId){
            var diary = context.DiaryList.Find(diaryId);
            if (diary == null){
                return NotFound();
            }
            return Ok(context.StyleList.Where(x => x.DiaryId == diaryId));
        }
    }
}