using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DiariesController(DiariesRepo repo) : ControllerBase()
    {
        private readonly DiariesRepo repo = repo; 

        // Get päring, et saada kõik päevikud, kätte
        [HttpGet]
        public async Task<IActionResult> GetAll(){
            var result = await repo.GetAllDiaries();
            return Ok(result);
        }

        // Get päring, et saada kätte id põhjal üks päevik
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDiary(int id){
            var diary = await repo.GetDiaryById(id);
            if (diary == null){
                return NotFound();
            }
            return Ok(diary);
        }

        // Post päring, et salvestada andmebaasi uus päevik
        [HttpPost]
        public async Task<IActionResult> SaveDiary([FromBody] Diary diary){
            var diaryExists = await repo.DiaryExistsInDb(diary.Id);
            if (diaryExists){
                return Conflict();
            }
            var result = await repo.SaveDiaryToDb(diary);
            return CreatedAtAction(nameof(SaveDiary), new { diary.Id }, result);  // Tagasta loodud päevik
        }


        // Put päring, et uuendada päevikut id põhjal
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Diary diary) {
            bool result = await repo.UpdateDiary(id, diary);
            return result ? NoContent() : NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDiary(int id){
            var diaryExists = await repo.DiaryExistsInDb(id);
            if (!diaryExists){
                return NotFound(); // Tagasta 404, kui päevikut ei leidut
            }
            bool deleted = await repo.DeleteDiaryById(id);
            if (deleted){
                return NoContent(); // Tagasta 204 No Content, kui kustutamine õnnestus
            }

            return StatusCode(500, "Internal server error while trying to delete the post."); // Tagasta 500, kui ilmneb viga
        }
    }
}