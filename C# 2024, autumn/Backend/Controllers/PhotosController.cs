using Microsoft.AspNetCore.Mvc;
using ITB2203_2024_traveldiary.Models.Classes;
using ITB2203_2024_traveldiary.Data.Repos;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace ITB2203_2024_traveldiary.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly PhotosRepo repo;
        private readonly PostsRepo postsRepo;

        public PhotosController(PhotosRepo repo, PostsRepo postsRepo)
        {
            this.repo = repo;
            this.postsRepo = postsRepo;
        }

        // Üksik foto salvestamine
        [HttpPost]
        public async Task<IActionResult> SavePhoto([FromBody] Photo photo)
        {
            var photoExists = await repo.PhotoExistsInDb(photo.Id);
            if (photoExists)
            {
                return Conflict();
            }

            var result = await repo.SavePhotoToDb(photo);
            return CreatedAtAction(nameof(SavePhoto), new { photo.Id }, result);
        }

        

        // Kõikide fotode saamine
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await repo.GetAllPhotos();
            return Ok(result);
        }

        // Foto üleslaadimine
        [HttpPost("upload")]
        public async Task<IActionResult> UploadPhoto([FromForm] IFormFile file, [FromForm] int postId)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest("No file provided.");
            }

            try
            {
                // Salvesta pilt
                var photo = new Photo
                {
                    ImageSrc = await SaveFile(file), // Meetod, mis salvestab faili ja tagastab tee
                };

                var savedPhoto = await repo.SavePhotoToDb(photo);
                Console.WriteLine($"Saved Photo: {savedPhoto.ImageSrc}"); // Lisa log

                // Seosta pilt postitusega
                var post = await postsRepo.GetPostById(postId);
                if (post == null)
                {
                    return NotFound("Post not found.");
                }
                Console.WriteLine($"Found Post: {post.Title}, Photos Count: {post.Photos?.Count}");

                // post.Photos ??= new List<Photo>();
                // post.Photos.Add(savedPhoto);

                await postsRepo.UpdatePost(post.Id, post);
                Console.WriteLine($"Post Updated: Photos Count After Update: {post.Photos?.Count}");

                return Ok(savedPhoto); // Tagasta salvestatud pilt
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error saving photo: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        private async Task<string> SaveFile(IFormFile file)
        {
            var uploadsFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images");
            if (!Directory.Exists(uploadsFolder))
            {
                Directory.CreateDirectory(uploadsFolder);
            }

            var uniqueFileName = $"{Guid.NewGuid()}_{file.FileName}";
            var filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return $"/images/{uniqueFileName}"; // Tagasta unikaalne faili asukoht
        }





        // Ühe foto saamine
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhoto(int id)
        {
            var photo = await repo.GetPhotoById(id);
            if (photo == null)
            {
                return NotFound();
            }
            return Ok(photo);
        }

        // Foto kustutamine
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhoto(int id)
        {
            var photoExists = await repo.PhotoExistsInDb(id);
            if (!photoExists)
            {
                return NotFound(); // Tagasta 404, kui foto ei leidu
            }
            bool deleted = await repo.DeletePhotoById(id);
            if (deleted)
            {
                return NoContent(); // Tagasta 204 No Content, kui kustutamine õnnestus
            }

            return StatusCode(500, "Internal server error while trying to delete the photo."); // Tagasta 500, kui ilmneb viga
        }

    }
}
