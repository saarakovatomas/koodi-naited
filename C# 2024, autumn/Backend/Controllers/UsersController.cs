using BackEnd.Models.Classes;
using ITB2203_2024_traveldiary.Data.Repos;
using Microsoft.AspNetCore.Mvc;


namespace ITB2203_2024_traveldiary.Controllers;

     [Route("api/[controller]")]
    public class UsersController(UsersRepo repo) : ControllerBase()
    {
        private readonly UsersRepo repo = repo;

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] User login)
        {
            var token = await repo.Login(login);

            if (!string.IsNullOrEmpty(token))
            {
                return Ok(new { Token = token });
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] User user)
        {
            if (string.IsNullOrWhiteSpace(user.Username) ||
                string.IsNullOrWhiteSpace(user.Password) || // Vana parool
                string.IsNullOrWhiteSpace(user.NewPassword)) // Uus parool
            {
                return BadRequest(new { Message = "All fields are required" });
            }

            var isPasswordChanged = await repo.ChangePassword(user.Username, user.Password, user.NewPassword);

            if (isPasswordChanged)
            {
                return Ok(new { Message = "Password changed successfully" });
            }
            else
            {
                return BadRequest(new { Message = "Invalid username or old password" });
            }  
         }   

         [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] User newUser)
        {
            var isRegistered = await repo.Register(newUser);

            if (isRegistered)
            {
                return Ok(new { Message = "Registration successful" });
            }
            else
            {
                return BadRequest(new { Message = "Username already exists" });
            }       
        } 

         [HttpGet("user/{username}")]
         public async Task<IActionResult> GetUserByUsername(string username)
          {
               var user = await repo.GetUserByUsername(username);

           if (user == null)
            {
                return NotFound("User not found");
            }

                return Ok(user);
            }




    }
