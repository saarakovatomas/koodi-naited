using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class ChecklistsController(ChecklistsRepo repo) : ControllerBase
    {
        private readonly ChecklistsRepo repo = repo;

        // GET päring kõigi nimekirjade toomiseks
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await repo.GetAllChecklists(); 
            return Ok(result); 
        }

        // GET päring konkreetse nimekirja toomiseks ID järgi
        [HttpGet("{id}")]
        public async Task<IActionResult> GetChecklist(int id)
        {
            var checklist = await repo.GetChecklistById(id); // Leia nimekiri ID alusel
            if (checklist == null)
            {
                return NotFound(); 
            }
            return Ok(checklist); 
        }

        // POST päring uue nimekirja salvestamiseks andmebaasi
        [HttpPost]
        public async Task<IActionResult> SaveChecklist([FromBody] Checklist checklist)
        {
            var checklistExists = await repo.ChecklistExistsInDb(checklist.Id); // Kontrolli, kas nimekiri juba eksisteerib
            if (checklistExists)
            {
                return Conflict(); 
            }
            var result = await repo.SaveChecklistToDb(checklist); // Salvesta uus nimekiri andmebaasi
            return CreatedAtAction(nameof(GetChecklist), new { id = checklist.Id }, result); 
        }

        // PUT päring olemasoleva nimekirja uuendamiseks ID järgi
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Checklist checklist)
        {
            if (id != checklist.Id)
            {
                return BadRequest("ID URL-is ei ühti keha ID-ga."); // Tagasta HTTP 400 vastus, kui ID-d ei kattu
            }

            bool result = await repo.UpdateChecklist(id, checklist); // Uuenda nimekirja andmebaasis
            return result ? NoContent() : NotFound(); // Tagasta HTTP 204, kui edukas, või 404, kui nimekirja ei leitud
        }
        
        // DELETE päring nimekirja kustutamiseks ID järgi
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChecklist(int id)
        {
            var checklistExists = await repo.ChecklistExistsInDb(id); // Kontrolli, kas nimekiri eksisteerib
            if (!checklistExists)
            {
                return NotFound(); 
            }
            bool deleted = await repo.DeleteChecklistById(id); // Kustuta nimekiri andmebaasist
            if (deleted)
            {
                return NoContent(); 
            }

            return StatusCode(500, "Sisemine serveri viga nimekirja kustutamisel."); // Tagasta HTTP 500, kui kustutamisel tekkis viga
        }
    }

}