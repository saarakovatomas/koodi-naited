using BackEnd.Data.Repos;
using BackEnd.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class BudgetsController(BudgetsRepo repo) : ControllerBase()
    {
        private readonly BudgetsRepo repo = repo; 

        [HttpGet]
        public async Task<IActionResult> GetAll(){
            var result = await repo.GetAllBudgets();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBudget(int id){
            var post = await repo.GetBudgetById(id);
            if (post == null){
                return NotFound();
            }
            return Ok(post);
        }

        [HttpPost]
        public async Task<IActionResult> SaveBudget([FromBody] Budget budget){
            var budgetExists = await repo.BudgetExistsInDb(budget.Id);
            if (budgetExists){
            return Conflict();
            }
         var result = await repo.SaveBudgetToDb(budget); // Await the async method
         return CreatedAtAction(nameof(SaveBudget), new { result.Id }, result);
        }
        // Put päring, et uuendada postituse id põhjal
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Budget budget) {
            bool result = await repo.UpdateBudget(id, budget);
            return result ? NoContent() : NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBudget(int id){
            var budgetExists = await repo.BudgetExistsInDb(id);
            if (!budgetExists){
                return NotFound(); // Tagasta 404, kui postitust ei leidu
            }
            bool deleted = await repo.DeleteBudgetById(id);
            if (deleted){
                return NoContent(); // Tagasta 204 No Content, kui kustutamine õnnestus
            }
            return StatusCode(500, "Internal server error while trying to delete the post."); // Tagasta 500, kui ilmneb viga
        }
        
    }
}