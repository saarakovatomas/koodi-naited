using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PostsController(PostsRepo repo) : ControllerBase()
    {
        private readonly PostsRepo repo = repo;

        // Get päring, et saada kõik postitused kätte
        [HttpGet]
        public async Task<IActionResult> GetAll(){
            var result = await repo.GetAllPosts();
            return Ok(result);
        }

        // Get päring, et saada kätte id põhjal üks postitus
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPost(int id){
            var post = await repo.GetPostById(id);
            if (post == null){
                return NotFound();
            }
            return Ok(post);
        }

        // Post päring, et salvestada andmebaasi uus postitus
        [HttpPost]
        public async Task<IActionResult> SavePost([FromBody] Post post){
            var postExists = await repo.PostExistsInDb(post.Id);
            if (postExists){
                return Conflict();
            }
            var result = repo.SavePostToDb(post);
            return CreatedAtAction(nameof(SavePost), new {post.Id}, result);
        }

        // Put päring, et uuendada postituse id põhjal
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Post post)
        {
            var existingPost = await repo.GetPostById(id);
            if (existingPost == null)
            {
                return NotFound();
            }

            // Uuenda ainult saadud väljad
            existingPost.Title = post.Title ?? existingPost.Title;
            existingPost.Content = post.Content ?? existingPost.Content;
            existingPost.Location = post.Location ?? existingPost.Location;
            existingPost.DateTime = post.DateTime ?? existingPost.DateTime;
            existingPost.Photos = post.Photos ?? existingPost.Photos;
            existingPost.Layout = post.Layout ?? existingPost.Layout;

            var result = await repo.UpdatePost(id, existingPost);
            return result ? NoContent() : StatusCode(500, "Failed to update post");
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id){
            var postExists = await repo.PostExistsInDb(id);
            if (!postExists){
                return NotFound(); // Tagasta 404, kui postitust ei leidu
            }
            bool deleted = await repo.DeletePostById(id);
            if (deleted){
                return NoContent(); // Tagasta 204 No Content, kui kustutamine õnnestus
            }

            return StatusCode(500, "Internal server error while trying to delete the post."); // Tagasta 500, kui ilmneb viga
        }

        [HttpPost("{id}/photos")]
        public async Task<IActionResult> AddPhotoToPost(int id, [FromBody] string photoUrl)
        {
            var post = await repo.GetPostById(id);
            if (post == null)
            {
                return NotFound();
            }

            // Kontrollige, et photoUrl ei oleks tühi
            if (string.IsNullOrWhiteSpace(photoUrl))
            {
                return BadRequest("Photo URL cannot be empty.");
            }

            post.Photos = post.Photos ?? new List<string>();

            // Lisa foto ainult siis, kui see ei ole tühi string
            if (!string.IsNullOrWhiteSpace(photoUrl))
            {
                post.Photos.Add(photoUrl);
            }

            // Puhasta Photos massiiv tühjadest stringidest
            post.Photos = post.Photos.Where(photo => !string.IsNullOrWhiteSpace(photo)).ToList();

            var updated = await repo.UpdatePost(id, post);
            return updated ? Ok(post) : StatusCode(500, "Unable to update the post with new photo");
        }


        // [HttpPost("{id}/upload-photo")]
        // public async Task<IActionResult> UploadPhoto(int id, [FromForm] IFormFile photo)
        // {
        //     // Leia postitus ID järgi
        //     var post = await repo.GetPostById(id);
        //     if (post == null)
        //     {
        //         return NotFound("Post not found");
        //     }

        //     // Kontrolli, kas fail on saadetud
        //     if (photo == null || photo.Length == 0)
        //     {
        //         return BadRequest("Invalid photo file");
        //     }

        //     // Määra privaatkaust fotode salvestamiseks
        //     var privateFolder = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())!.FullName, "Backend", "uploads");
        //     Directory.CreateDirectory(privateFolder);

        //     // Failinime sanitiseerimine
        //     var sanitizedFileName = Path.GetFileNameWithoutExtension(photo.FileName)
        //         .Replace(" ", "_")
        //         .Replace("õ", "o")
        //         .Replace("ä", "a")
        //         .Replace("ö", "o")
        //         .Replace("ü", "u") + Path.GetExtension(photo.FileName);

        //     var filePath = Path.Combine(privateFolder, sanitizedFileName);

        //     try
        //     {
        //         // Salvesta fail kausta
        //         using (var stream = new FileStream(filePath, FileMode.Create))
        //         {
        //             await photo.CopyToAsync(stream);
        //         }

        //         // Lisa failinimi andmebaasi
        //         post.Photos = post.Photos ?? new List<string>();
        //         post.Photos.Add(sanitizedFileName);

        //         var updated = await repo.UpdatePost(id, post);
        //         if (updated)
        //         {
        //             // Tagasta API kaudu ligipääsetav URL
        //             var photoUrl = $"/api/posts/photo/{sanitizedFileName}";
        //             return Ok(new { photoUrl });
        //         }
        //         return StatusCode(500, "Error updating the post with new photo");
        //     }
        //     catch (Exception ex)
        //     {
        //         return StatusCode(500, $"Error saving photo: {ex.Message}");
        //     }
        // }



        [HttpPost("{id}/upload-photo")]
        public async Task<IActionResult> UploadPhoto(int id, [FromForm] IFormFile photo)
        {
            var post = await repo.GetPostById(id);
            if (post == null)
            {
                return NotFound("Post not found");
            }

            if (photo == null || photo.Length == 0)
            {
                return BadRequest("Invalid photo file");
            }

            var frontendFolder = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())!.FullName, "Frontend", "uploads");
            Directory.CreateDirectory(frontendFolder);
            var sanitizedFileName = Path.GetFileNameWithoutExtension(photo.FileName)
                .Replace(" ", "_")
                .Replace("õ", "o")
                .Replace("ä", "a")
                .Replace("ö", "o")
                .Replace("ü", "u") + Path.GetExtension(photo.FileName);

            var filePath = Path.Combine(frontendFolder, sanitizedFileName);


            try
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await photo.CopyToAsync(stream);
                }

                //var photoUrl = Path.Combine("uploads", sanitizedFileName).Replace("\\", "/");
                var photoUrl = sanitizedFileName;

                post.Photos = post.Photos ?? new List<string>();
                post.Photos.Add(photoUrl);

                // Puhasta Photos massiiv tühjadest stringidest
                post.Photos = post.Photos.Where(photo => !string.IsNullOrWhiteSpace(photo)).ToList();

                var updated = await repo.UpdatePost(id, post);
                if (updated)
                {
                    return Ok(new { photoUrl });
                }
                return StatusCode(500, "Error updating the post with new photo");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error saving photo: {ex.Message}");
            }
        }

        [HttpGet("uploads")]
        public IActionResult GetUploads()
        {
            var uploadsFolderPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())!.FullName, "Frontend", "uploads");

            if (!Directory.Exists(uploadsFolderPath))
            {
                return NotFound("Uploads folder not found.");
            }

            var files = Directory.GetFiles(uploadsFolderPath)
                .Select(file => Path.GetFileName(file))
                .ToList();

            return Ok(files);
        }


        [HttpGet("photo/{fileName}")]
        public IActionResult GetPhoto(string fileName)
        {
            var privateFolder = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())!.FullName, "Backend", "uploads");
            var filePath = Path.Combine(privateFolder, fileName);

            if (!System.IO.File.Exists(filePath))
            {
                return NotFound("Photo not found");
            }

            var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var mimeType = "image/jpeg"; // Muuda vastavalt vajadusele MIME-tüüpi

            return File(fileStream, mimeType, fileName);
        }


    }
}