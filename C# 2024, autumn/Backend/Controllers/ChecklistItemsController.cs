using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class ChecklistItemsController(ChecklistItemsRepo repo) : ControllerBase
    {
        private readonly ChecklistItemsRepo repo = repo;

        // GET päring, et tuua kõik nimekirja punktid
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await repo.GetAllChecklistItems(); // Kutsu meetod, mis tagastab kõik nimekirja punktid
            return Ok(result);
        }

        // GET päring, et tuua üksik nimekirja punkt ID alusel
        [HttpGet("{id}")]
        public async Task<IActionResult> GetChecklistItem(int id)
        {
            var checklistItem = await repo.GetChecklistItemById(id); // Leia nimekirja punkt ID alusel
            if (checklistItem == null)
            {
                return NotFound(); 
            }
            return Ok(checklistItem);
        }

        // POST päring, et salvestada uus nimekirja punkt andmebaasi
        [HttpPost]
        public async Task<IActionResult> SaveChecklistItem([FromBody] ChecklistItem checklistItem)
        {
            var checklistItemExists = await repo.ChecklistItemExistsInDb(checklistItem.Id); 
            if (checklistItemExists)
            {
                return Conflict(); 
            }
            var result = await repo.SaveChecklistItemToDb(checklistItem); 
            return CreatedAtAction(nameof(GetChecklistItem), new { id = checklistItem.Id }, result); 
        }

        // PUT päring, et uuendada olemasolevat nimekirja punkti ID alusel
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] ChecklistItem checklistItem)
        {
            if (id != checklistItem.Id)
            {
                return BadRequest("URL-is olev ID ei ühti keha ID-ga."); // Kontrolli, kas ID-d sobivad
            }

            bool result = await repo.UpdateChecklistItem(id, checklistItem); // Uuenda nimekirja punkti andmebaasis
            return result ? NoContent() : NotFound(); 
        }

        // DELETE päring, et kustutada nimekirja punkt ID alusel
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChecklistItem(int id)
        {
            var checklistItemExists = await repo.ChecklistItemExistsInDb(id); 
            if (!checklistItemExists)
            {
                return NotFound(); 
            }
            bool deleted = await repo.DeleteChecklistItemById(id); 
            if (deleted)
            {
                return NoContent(); 
            }

            return StatusCode(500, "Sisemine serveri viga nimekirja punkti kustutamisel."); 
        }
    }
}