using System.Threading.Tasks;
using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StylesController(StylesRepo repo) : ControllerBase()
    {
        private readonly StylesRepo repo = repo; 

        // Get päring, et saada kõik stiilid kätte
        [HttpGet]
        public async Task<IActionResult> GetAll(){
            var result = await repo.GetAllStyles();
            return Ok(result);
        }

        // Get päring, et saada kätte id põhjal üks stiil
        [HttpGet("{id}")]
        public async Task<IActionResult> GetStyle(int id){
            var style = await repo.GetStyleById(id);
            if (style == null){
                return NotFound();
            }
            return Ok(style);
        }

        // Post päring, et salvestada andmebaasi uus stiil
        [HttpPost]
        public async Task<IActionResult> SaveStyle([FromBody] Style style){
            var styleExists = await repo.StyleExistsInDb(style.Id);
            if (styleExists){
                return Conflict();
            }
            var result = repo.SaveStyleToDb(style);
            return CreatedAtAction(nameof(SaveStyle), new {style.Id}, result);
        }

        // Put päring, et uuendada stiili id põhjal
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Style style) {
            bool result = await repo.UpdateStyle(id, style);
            return result ? NoContent() : NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStyle(int id){
            var styleExists = await repo.StyleExistsInDb(id);
            if (!styleExists){
                return NotFound(); // Tagasta 404, kui stiili ei leidu
            }
            bool deleted = await repo.DeleteStyleById(id);
            if (deleted){
                return NoContent(); // Tagasta 204 No Content, kui kustutamine õnnestus
            }

            return StatusCode(500, "Internal server error while trying to delete the style."); // Tagasta 500, kui ilmneb viga
        }
    }
}