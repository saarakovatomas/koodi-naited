using ITB2203_2024_traveldiary.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{
    [ApiController]
    [Route("api/diaries/{diaryId:int}/posts")]
    public class DiariesPostsController(DataContext context) : ControllerBase()
    {
        private readonly DataContext context = context;

        [HttpGet]
        public IActionResult GetDiaryPosts(int diaryId){
            var diary = context.DiaryList.Find(diaryId);
            if (diary == null){
                return NotFound();
            }
            return Ok(context.PostList.Where(x => x.DiaryId == diaryId));
        }
    }
}