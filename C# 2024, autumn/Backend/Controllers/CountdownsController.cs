using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITB2203_2024_traveldiary.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountdownsController : ControllerBase
    {
        private readonly CountdownsRepo _repo;

        public CountdownsController(CountdownsRepo repo)
        {
            _repo = repo;
        }

        // Get päring, et saada kõik loendurid
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _repo.GetAllCountdowns();
            return Ok(result);
        }

        // Get päring, et saada üks loendur ID alusel
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCountdown(int id)
        {
            var countdown = await _repo.GetCountdownById(id);
            if (countdown == null)
            {
                return NotFound();
            }
            return Ok(countdown);
        }

        // Post päring, et salvestada uus loendur
        [HttpPost]
        public async Task<IActionResult> SaveCountdown([FromBody] Countdown countdown)
        {
            // Valideerimine enne salvestamist
            if (countdown.TargetDate <= DateTime.UtcNow)
            {
                return BadRequest("TargetDate must be in the future.");
            }

            var countdownExists = await _repo.CountdownExistsInDb(countdown.Id);
            if (countdownExists)

            {
                return Conflict();
            }
            countdown.TargetDate = DateTime.SpecifyKind(countdown.TargetDate, DateTimeKind.Utc); // Määra UTC ajavöönd
            var result = await _repo.SaveCountdownToDb(countdown);
            return CreatedAtAction(nameof(SaveCountdown), new { countdown.Id }, result);  // Tagasta loodud loendur
        }

        // Put päring, et uuendada loendurit ID alusel
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Countdown countdown)
        {
            // Valideerimine enne uuendamist
            if (countdown.TargetDate <= DateTime.UtcNow)
            {
                return BadRequest("TargetDate must be in the future.");
            }

            bool result = await _repo.UpdateCountdown(id, countdown);
            return result ? NoContent() : NotFound();
        }

        // Delete päring, et kustutada loendur ID alusel
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCountdown(int id)
        {
            var countdownExists = await _repo.CountdownExistsInDb(id);
            if (!countdownExists)
            {
                return NotFound(); // Tagasta 404, kui loendurit ei leitud
            }
            bool deleted = await _repo.DeleteCountdownById(id);
            if (deleted)
            {
                return NoContent(); // Tagasta 204 No Content, kui kustutamine õnnestus
            }

            return StatusCode(500, "Internal server error while trying to delete the countdown."); // Tagasta 500, kui ilmneb viga
        }
    }
}
