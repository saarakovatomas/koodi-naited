using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using ITB2203_2024_traveldiary.Data.Repos;
using ITB2203_2024_traveldiary.Data;
using Microsoft.EntityFrameworkCore;
using BackEnd.Data.Repos;
using Microsoft.Extensions.FileProviders;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();
// Lubame staatilised failid
builder.Services.AddControllersWithViews();
builder.Services.AddDirectoryBrowser();

builder.Services
    //.AddDbContext<DataContext>(options => options.UseInMemoryDatabase("restDB"))
    .AddDbContext<DataContext>(options => options.UseNpgsql(builder.Configuration.GetConnectionString("Default")))
    .AddScoped<DiariesRepo>()
    .AddScoped<PostsRepo>()
    .AddScoped<PhotosRepo>()
    .AddScoped<BudgetsRepo>()
    .AddScoped<CountdownsRepo>()
    .AddScoped<ChecklistsRepo>()
    .AddScoped<ChecklistItemsRepo>()
    .AddScoped<StylesRepo>()
    .AddScoped<UsersRepo>();
    

builder.Services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
    {
        builder
        .SetIsOriginAllowed(_ => true)
        .AllowCredentials()
        .AllowAnyMethod()
        .AllowAnyHeader();
    }));

    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = builder.Configuration["Jwt:Issuer"],
            ValidAudience = builder.Configuration["Jwt:Issuer"],
            IssuerSigningKey = new 
            SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]!))
        };
    });




var app = builder.Build();

using (var scope = ((IApplicationBuilder)app).ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
using (var context = scope.ServiceProvider.GetService<DataContext>())
{
    context?.Database.EnsureCreated();
}

// Enable serving static files
app.UseStaticFiles();

var uploadsPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())!.FullName, "Frontend", "uploads");

// Kontrolli, kas kaust eksisteerib; kui ei, loo see
if (!Directory.Exists(uploadsPath))
{
    Directory.CreateDirectory(uploadsPath);
}

app.UseStaticFiles(new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(uploadsPath),
    RequestPath = "/uploads"
});

app.UseDirectoryBrowser(); // Lubame kaustade sirvimise (nt "Uploads")


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseCors("MyPolicy");

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.UseRouting();

app.MapControllers();

app.Run();


